#include "Arduino.h"
#include "config.h"

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <string.h>

#include "AccelStepper.h"
#include "Logger.h"
#include "HMI.h"
#include "Actuator.h"
#include "Carousel.h"
#include "RobotArm.h"
#include "Singulator.h"
#include "Sharpener.h"

#include "Emergency.h"

HMI* hmi;
Actuator* actuator;
Carousel* carousel;
RobotArmControl* robotArmControl;
Singulator* singulator;
Sharpener* sharpener;
EmergencyStopButton* eStopButton;

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

volatile boolean PAUSE;
volatile boolean ESTOP_PREVIOUSLY_TRIGGERED;
volatile boolean EMERGENCY;
volatile long long last = 0;
volatile long long now;

void (*resetArduino)(void) = 0;

void setupHMI();
void setupActuator();
void setupCarousel();
void setupRobotArm();
void setupSingulator();
void setupSharpener();
void initializeHMI();
void initializeActuator();
void initializeCarousel();
void initializeRobotArm();
void initializeSharpener();
void initializeSingulator();
void initialize();
void runFullCycle();
void runDryCycle();
void resetSystem();
void testActuator();
void testCarousel();
void testRobotArm();
void testSharpener();
void testSingulator();
void testRepeatabilityOfRobotArm();
void determineMaximumPayloadOfRobotArm();
void determineMinimumICOfRobotArmUnderMP();
void testMinimumAndMaximumWEOfRobotArm();
void testEmergencyStop();
void testErrorRecovery();

void load(uint8_t n);
void sharpen(uint8_t n);
void unload(uint8_t n);
void orderPencil();
void emergencyISR();
void emergencyRoutine();
void pauseISR();
void resetISR();
void pauseRoutine();
void triggerStartSwitch();
void moveToMaintanencePosition();

boolean once[] = {false, false, false};
short key = -1;

void setup() {
  Serial.begin(9600);
  
  ESTOP_PREVIOUSLY_TRIGGERED = false;
  EMERGENCY = false;
  PAUSE = false;
  
  
  setupCarousel();
  setupRobotArm();
  setupSharpener();
  setupSingulator();
  setupActuator();
  setupEmergencyRoutine();
  setupHMI();  
  
  initializeHMI();
  delay(1000);
  initializeCarousel();
  delay(1000);
  initializeRobotArm();
  delay(1000);
  initializeSharpener();
  delay(1000);
  initializeActuator();
  delay(1000);
  initializeSingulator();
  delay(1000);

  triggerStartSwitch();
}

void loop() {  
  
  if (hmi->startButton->isPressed() && robotArmControl->start) {  
    hmi->update();
    key = hmi->select();
    if (key == 0 || key == 1 || key == 2 || key == 3) {
      hmi->mode = key;
    }
  
    switch (hmi->mode) {
      case MAIN_MODE:
        switch (key) {
          case 4:
            orderPencil();
            break;
          default:
            break;
        }
  
        break;
      case MAINTANENCE_MODE:
        switch (key) {
          case 5:
            runDryCycle();
            break;
          case 6:
            moveToMaintanencePosition();
            break;
          case 7:
            initialize();
            break;
          case 8:
            initializeActuator();
            break;
          case 9:
            initializeCarousel();
            break;
          case 10:
            initializeRobotArm();
            break;
          case 11:
            initializeSharpener();
            break;
          case 12:
            initializeSingulator();
            break;
          case 13:
            initializeHMI();
            break;
          default:
            break;
        };
        break;
      case TEST_MODE:
        switch (key) {
          case 14:
            testActuator();
            break;
          case 15:
            testCarousel();
            break;
          case 16:
            testRobotArm();
            break;
          case 17:
            testSharpener();
            break;
          case 18:
            testSingulator();
            break;
          case 19:
            testRepeatabilityOfRobotArm();
            break;
          case 20:
            // determineMaximumPayloadOfRobotArm();
            break;
          case 21:
            // determineMinimumICOfRobotArmUnderMP();
            break;
          case 22:
            // testMinimumAndMaximumWEOfRobotArm();
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }


    once[0] = false;
    
  } else {
    if (! once[0]) {
      once[0] = true;
      hmi->mainDisplay->clear();
      hmi->mainDisplay->setCursor(0, 0);
      hmi->mainDisplay->print("PencilBot v1.0");
      
      hmi->statusDisplay->clear();
      hmi->statusDisplay->setCursor(0, 0);
      hmi->statusDisplay->print("Press green btn");  
      hmi->statusDisplay->setCursor(0, 1);
      hmi->statusDisplay->print("to start");  
    }
  }
}

void setupHMI() {
  println("Set up HMI");

  hmi = new HMI();

  hmi->mainDisplay = new LiquidCrystal_I2C(0x26, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
  hmi->statusDisplay = new LiquidCrystal_I2C(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

  hmi->startButton = new Button(26);
  hmi->pauseButton = new Button(18);
  attachInterrupt(digitalPinToInterrupt(18), pauseISR, CHANGE);
  delay(3000);
  PAUSE = hmi->pauseButton->isPressed();
  if (PAUSE) {
//    pauseRoutine(); 
  }
  hmi->resetButton = new Button(3);
  attachInterrupt(digitalPinToInterrupt(3), resetISR, CHANGE);
  delay(3000);
  hmi->upButton = new Button(24);
  hmi->downButton = new Button(23);
  hmi->selectButton = new Button(27);
  hmi->backButton = new Button(22);

  hmi->menu = new Menu();
  println("HMI is set");
}

void setupActuator() {
  println("Set up Actuator");
  /* parameters:
         <type number of a driver>,
         <pin number to STEP>
         <pin number to DIR (direction)>
     for example:
        1 is 2 pin driver declaration
        Pin 2 connected to STEP pin
        Pin 3 connected to DIR pin
  */
  actuator = new Actuator(0, 2000);
  actuator->stepperMotor = new AccelStepper(1, 7, 6);
  actuator->stepperMotor->setMaxSpeed(300.0);
  println("Max speed of Actuator is set");
  actuator->stepperMotor->setAcceleration(300.0);
  println("Acceleration of Actuator is set");
  actuator->stepperMotor->setSpeed(300.0);

  actuator->gripper = new Gripper(6, 0, 90, 320, 650); // parameters: #pin, #minAngle, #maxAngle
  actuator->gripper->pwmDriver = &pwm;
  println("Gripper is set");

  println("Set home limit switch of Actuator");
  actuator->homeLimitSwitch = new LimitSwitch(46); // parameters: #pin

  println("Home limit switch of Actuator is set");

  println("Actuator is set");
}

void setupCarousel() {
  println("Set up Carousel");
  carousel = new Carousel(0, 40); // parameters: #homePosition, #slot
  carousel->stepperMotor = new AccelStepper(1, 5, 4); // parameters: #step, #dir
  carousel->stepperMotor->setMaxSpeed(1000.0);
  println("Max speed of Carousel is set");
  carousel->stepperMotor->setAcceleration(300.0);
  println("Acceleration of Carousel is set");
  carousel->stepperMotor->setSpeed(300.0);
  println("Speed of Carousel is set");

  println("Set infrared sensor of Carousel");
  carousel->infraredSensor = new InfraredSensor(50, A0);
  println("Infrared sensor of Carousel is set");

  println("Carousel is set");
}

void setupRobotArm() {
  println("Set up RobotArmControl");

  robotArmControl = new RobotArmControl();
  println("Initialize Adafruit_PWMServoDriver");
  // called this way, it uses the default address 0x40

  pwm.begin();
  pwm.setPWMFreq(100);  // Analog servos run at ~100 Hz updates
  println("Adafruit_PWMServoDriver is set");

  println("Robot Arm: set up Base");
  robotArmControl->base.pwmDriver = &pwm;
  robotArmControl->base.minAngle = 0;
  robotArmControl->base.maxAngle = 180;
  robotArmControl->base.channel.number = 0;
  robotArmControl->base.channel.minPulseWidth = 225;
  robotArmControl->base.channel.maxPulseWidth = 910;
  robotArmControl->base.setDefaultPulseWidth(567);
  robotArmControl->base.stepDelay = 15; // milliseconds
  println("Robot Arm: Base is set");

  println("Robot Arm: set up Shoulder");
  robotArmControl->shoulder.pwmDriver = &pwm;
  robotArmControl->shoulder.minAngle = 30;
  robotArmControl->shoulder.maxAngle = 150;
  robotArmControl->shoulder.channel.number = 1;
  robotArmControl->shoulder.channel.minPulseWidth = 250;
  robotArmControl->shoulder.channel.maxPulseWidth = 800;
  robotArmControl->shoulder.setDefaultPulseWidth(520);
  robotArmControl->shoulder.stepDelay = 50; // milliseconds
  println("Robot Arm: Shoulder is set");

  println("Robot Arm: set up UpperArm");
  robotArmControl->upperArm.pwmDriver = &pwm;
  robotArmControl->upperArm.minAngle = 0;
  robotArmControl->upperArm.maxAngle = 180;
  robotArmControl->upperArm.channel.number = 2;
  robotArmControl->upperArm.channel.minPulseWidth = 145;
  robotArmControl->upperArm.channel.maxPulseWidth = 750;
  robotArmControl->upperArm.setDefaultPulseWidth(415);
  robotArmControl->upperArm.stepDelay = 15; // milliseconds
  println("Robot Arm: UpperArm is set");

  println("Robot Arm: set up LowerArm");
  robotArmControl->lowerArm.pwmDriver = &pwm;
  robotArmControl->lowerArm.minAngle = 0;
  robotArmControl->lowerArm.maxAngle = 180;
  robotArmControl->lowerArm.channel.number = 3;
  robotArmControl->lowerArm.channel.minPulseWidth = 250;
  robotArmControl->lowerArm.channel.maxPulseWidth = 905;
  robotArmControl->lowerArm.setDefaultPulseWidth(390);
  robotArmControl->lowerArm.stepDelay = 15; // milliseconds
  println("Robot Arm: LowerArm is set");

  println("Robot Arm: set up wrist");
  robotArmControl->wrist.pwmDriver = &pwm;
  robotArmControl->wrist.minAngle = 0;
  robotArmControl->wrist.maxAngle = 180;
  robotArmControl->wrist.channel.number = 4;
  robotArmControl->wrist.channel.minPulseWidth = 155;
  robotArmControl->wrist.channel.maxPulseWidth = 860;
  robotArmControl->wrist.setDefaultPulseWidth(505);
  robotArmControl->wrist.stepDelay = 15; // milliseconds
  println("Robot Arm: Wrist is set");

  println("Robot Arm: set up ArmGripper");
  robotArmControl->armGripper.pwmDriver = &pwm;
  robotArmControl->armGripper.minAngle = 0; // 90 - 155 degrees are working range
  robotArmControl->armGripper.maxAngle = 60;
  robotArmControl->armGripper.channel.number = 5;
  robotArmControl->armGripper.channel.minPulseWidth = 240;
  robotArmControl->armGripper.channel.maxPulseWidth = 470;
  robotArmControl->armGripper.setDefaultPulseWidth(469);
  robotArmControl->armGripper.stepDelay = 15; // milliseconds
  println("Robot Arm: ArmGripper is set");

  println("Set start switch");
  robotArmControl->startSwitch = new LimitSwitch(34);
  println("Start switch is set");
  
  println("RobotArmControl is set");
}

void setupSingulator() {
  println("Set up singulator");
  singulator = new Singulator(); // parameters: #homePosition, #slot
  singulator->stepperMotor = new AccelStepper(8, 8, 10, 9, 11); // parameters: #step, #dir
  
  singulator->stepperMotor->setMaxSpeed(1000.0);
  println("Max speed of singulator is set");
  singulator->stepperMotor->setAcceleration(100.0);
  println("Acceleration of singulator is set");
  singulator->stepperMotor->setSpeed(300.0);
  
  println("Speed of singulator is set");

  println("Set infrared sensors of singulator");
  singulator->infraredSensor1 = new InfraredSensor(51);
  singulator->infraredSensor2 = new InfraredSensor(50);
  println("Infrared sensor of singulator is set");

  println("Singulator is set");
}

void setupSharpener() {
  println("Set up sharpener");
  sharpener = new Sharpener(40);
  println("sharpener is set");
}

void setupEmergencyRoutine() {
  println("Set up emergency routine");
  eStopButton = new EmergencyStopButton(2, digitalPinToInterrupt(2));
  attachInterrupt(eStopButton->interruptPin, emergencyISR, CHANGE); 
  delay(1000);
  EMERGENCY = eStopButton->isPressed();
  Serial.print("E-stop:");
  Serial.println(EMERGENCY);
  if (EMERGENCY == true) {
    emergencyRoutine();
  }
}

void initializeHMI() {
  println("Initialize HMI");
  hmi->init();
  println("HMI is initialized");
}

void initializeActuator() {
  println("Initialize actuator");
  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Initializing");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("actuator");
  actuator->gripper->open();
  delay(700);
  actuator->moveToHomePosition();
  delay(700);
  actuator->gripper->open();
  delay(700);
  println("Actuator is initialized");
}

void initializeCarousel() {
  println("Initialize carousel");
  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Initializing");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("carousel");
  carousel->init();
  println("Carousel is initialized");
}

void initializeRobotArm() {
  println("Initialize robot arm");
  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Initializing");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("robot arm");
  robotArmControl->init();
  println("Default angles are set");
}

void initializeSharpener() {
  println("Initialize sharpener");
  sharpener->init();
  println("Initialization of sharpener is done");
}

void initializeSingulator() {
  println("Initialize singulator");
  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Initializing");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("singulator");
  singulator->init();
  println("Initialization of singulator is done");
}

void initialize() {
  println("Initialize system");
  initializeHMI();
  initializeActuator();
  initializeCarousel();
  initializeRobotArm();
  initializeSharpener();
  initializeSingulator();
}

void runDryCycle() {
  println("Run dry cycle");
  if (robotArmControl->initialized == false) {
    initializeRobotArm();  
  }

  if (actuator->initialized == false) {
    initializeActuator();
  }

  if (carousel->initialized == false) {
    initializeCarousel();
  }

  if (singulator->initialized == false) {
    initializeSingulator();
  }

  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Running");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("dry cycle");

  robotArmControl->base.stepDelay = 15 + 70; // milliseconds
  robotArmControl->shoulder.stepDelay = 50 + 70; // milliseconds
  robotArmControl->upperArm.stepDelay = 15 + 70; // milliseconds
  robotArmControl->lowerArm.stepDelay = 15 + 70; // milliseconds
  robotArmControl->wrist.stepDelay = 15 + 70; // milliseconds
  robotArmControl->armGripper.stepDelay = 15 + 70; // milliseconds

  actuator->stepperMotor->setMaxSpeed(100.0);
  actuator->stepperMotor->setAcceleration(100.0);
  carousel->stepperMotor->setMaxSpeed(500.0);
  carousel->stepperMotor->setAcceleration(100.0);
  carousel->stepperMotor->setSpeed(100.0);
  singulator->stepperMotor->setMaxSpeed(1000.0);
  singulator->stepperMotor->setAcceleration(100.0);
  singulator->stepperMotor->setSpeed(300.0);
  
  println("Move full rotation");
  singulator->moveFullRotation();
  delay(1000);
  println("Move to position A");
  robotArmControl->moveToPositionA();
  println("Close arm gripper");
  robotArmControl->armGripper.close();
  delay(1000);
  println("Move to position I1");
  robotArmControl->moveToPositionI1();
  println("Move to position B");
  robotArmControl->moveToPositionB();
  println("Open arm gripper");
  robotArmControl->armGripper.open();
  delay(1000);
  println("Move to position I2");
  robotArmControl->moveToPositionI2();
  println("Next slot");
  carousel->nextSlot();
  delay(3000);
  println("Next slot");
  carousel->nextSlot();
  delay(3000);
  println("Move to pick position");
  actuator->moveToPosition(2800);
  println("Close gripper");
  actuator->gripper->close();
  delay(1000);
  println("Move to pre-sharpenning position");
  actuator->moveToPosition(1000);
  println("Move slowly while sharpenning");
  actuator->moveToHomePosition();
  delay(2000);
  println("Move back to pre-sharpenning position");
  actuator->moveToPosition(1000);
  println("Move to place position");
  actuator->moveToPosition(2500);
  println("Open gripper");
  actuator->gripper->open();
  delay(1000);
  actuator->moveToHomePosition();
  delay(2000);
  carousel->nextSlot();
  delay(3000);
  carousel->nextSlot();
  delay(3000);
  println("Move to position C");
  robotArmControl->moveToPositionC();
  println("Close arm gripper");
  robotArmControl->armGripper.close();
  delay(3000);
  println("Move to position I2_1");
  robotArmControl->moveToPositionI2_1();
  println("Move to position D");
  robotArmControl->moveToPositionD();
  println("Open arm gripper");
  robotArmControl->armGripper.open();
  delay(3000);
  println("Move to position I3");
  robotArmControl->moveToPositionI3();

  robotArmControl->base.stepDelay = 15; // milliseconds
  robotArmControl->shoulder.stepDelay = 50; // milliseconds
  robotArmControl->upperArm.stepDelay = 15; // milliseconds
  robotArmControl->lowerArm.stepDelay = 15; // milliseconds
  robotArmControl->wrist.stepDelay = 15; // milliseconds
  robotArmControl->armGripper.stepDelay = 15; // milliseconds

  actuator->stepperMotor->setMaxSpeed(300.0);
  actuator->stepperMotor->setAcceleration(300.0);
  carousel->stepperMotor->setMaxSpeed(1000.0);
  carousel->stepperMotor->setAcceleration(300.0);
  carousel->stepperMotor->setSpeed(300.0);
  singulator->stepperMotor->setMaxSpeed(1000.0);
  singulator->stepperMotor->setAcceleration(100.0);
  singulator->stepperMotor->setSpeed(300.0);

}

void testActuator() {
  println("Test actuator");
  if (actuator->initialized == false) {
    hmi->statusDisplay->clear();
    hmi->statusDisplay->setCursor(0, 0);
    hmi->statusDisplay->print("Initializing");
    hmi->statusDisplay->setCursor(0, 1);
    hmi->statusDisplay->print("actuator");
    initializeActuator();
  }

  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Testing");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("actuator");

  actuator->moveToPosition(2800);
  println("Close gripper");
  actuator->gripper->close();
  delay(1000);
  println("Move to pre-sharpenning position");
  actuator->moveToPosition(1000);
  delay(100);
  actuator->stepperMotor->setMaxSpeed(100.0);
  actuator->stepperMotor->setAcceleration(100.0);
  println("Move slowly while sharpenning");
  actuator->moveToHomePosition();
  delay(2000);
  actuator->stepperMotor->setMaxSpeed(300.0);
  actuator->stepperMotor->setAcceleration(300.0);
  println("Move back to pre-sharpenning position");
  actuator->moveToPosition(1000);
  println("Move to place position");
  actuator->moveToPosition(2500);
  println("Open gripper");
  actuator->gripper->open();
  delay(1000);
  actuator->moveToHomePosition();
  delay(2000);
  println("Move to initial position");
//  actuator->moveToPosition(500);
//  delay(1000);
}
void testCarousel() {
  Serial.println("Test carousel ");
  if (! carousel->infraredSensor->isObject()) {
    initializeCarousel();
    delay(2000);
  }

  for (int i = 0; i < 5; i++) {
    carousel->nextSlot();
    delay(3000);
  }
}
void testRobotArm() {
  println("Test robot arm");

  if (robotArmControl->initialized == false) {
    hmi->statusDisplay->clear();
    hmi->statusDisplay->setCursor(0, 0);
    hmi->statusDisplay->print("Initializing");
    hmi->statusDisplay->setCursor(0, 1);
    hmi->statusDisplay->print("robot arm");
    initializeRobotArm();
  }

  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Testing");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("robot arm");
  println("Move to position A");
  robotArmControl->moveToPositionA();
  println("Close arm gripper");
  robotArmControl->armGripper.close();
  delay(5000);
  println("Move to position I1");
  robotArmControl->moveToPositionI1();

  println("Move to position B");
  robotArmControl->moveToPositionB();
  println("Open arm gripper");
  robotArmControl->armGripper.open();
  delay(1000);
  println("Move to position I2");
  robotArmControl->moveToPositionI2();

  println("Move to position C");
  robotArmControl->moveToPositionC();
  println("Close arm gripper");
  robotArmControl->armGripper.close();
  delay(3000);
  println("Move to position I2_1");
  robotArmControl->moveToPositionI2_1();
  println("Move to position D");
  robotArmControl->moveToPositionD();
  println("Open arm gripper");
  robotArmControl->armGripper.open();
  delay(3000);
  println("Move to position I3");
  robotArmControl->moveToPositionI3();
}
void testSharpener() {
  Serial.println("Test sharpener");
  if (sharpener->initialized == false) {
    hmi->statusDisplay->clear();
    hmi->statusDisplay->setCursor(0, 0);
    hmi->statusDisplay->print("Initializing");
    hmi->statusDisplay->setCursor(0, 1);
    hmi->statusDisplay->print("sharpener");
    initializeSharpener();
  }
  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Testing");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("sharpener");
  sharpener->triggerON();
  delay(10000);
  sharpener->triggerOFF();
  delay(10000);
}
void testSingulator() {
  println("Test singulator");
  if (singulator->initialized == false) {
    hmi->statusDisplay->clear();
    hmi->statusDisplay->setCursor(0, 0);
    hmi->statusDisplay->print("Initializing");
    hmi->statusDisplay->setCursor(0, 1);
    hmi->statusDisplay->print("singulator");
    initializeSingulator();
  }

  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Testing");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("singulator");
  println("Move full rotation");
  singulator->moveFullRotation();
  delay(1000);
}
void testRepeatabilityOfRobotArm() {
  Serial.println("Test repeatability of robot arm");
  robotArmControl->armGripper.moveToDefaultAngle(); //100 degrees
  delay(1000);
  robotArmControl->wrist.moveToDefaultAngle(); // 90 degrees
  delay(1000);
  robotArmControl->lowerArm.moveToDefaultAngle(); // 105 degrees
  delay(1000);
  robotArmControl->upperArm.moveToDefaultAngle(); // 83 degrees
  delay(1000);
  robotArmControl->shoulder.moveToDefaultAngle(); // 86 degrees
  delay(1000);
  robotArmControl->base.moveToDefaultAngle(); // 90 degrees
  delay(1000);

  delay(2000);
  // position 0
  robotArmControl->armGripper.close();
  robotArmControl->wrist.stepToAngle(90);
  robotArmControl->lowerArm.stepToAngle(105);
  robotArmControl->upperArm.stepToAngle(83);
  robotArmControl->shoulder.stepToAngle(86);
  robotArmControl->base.stepToAngle(90);
  delay(1000);

  // position 1
  robotArmControl->base.stepToAngle(1);
  robotArmControl->shoulder.stepToAngle(95);
  robotArmControl->upperArm.stepToAngle(130);
  robotArmControl->lowerArm.stepToAngle(170);
  robotArmControl->wrist.stepToAngle(95);
  delay(1000);

  // position 1
  robotArmControl->base.stepToAngle(1);
  robotArmControl->shoulder.stepToAngle(95);
  robotArmControl->upperArm.stepToAngle(130);
  robotArmControl->lowerArm.stepToAngle(170);
  delay(1000);

  // position 1.1
  robotArmControl->shoulder.stepToAngle(95);
  robotArmControl->lowerArm.stepToAngle(179);
  robotArmControl->upperArm.stepToAngle(166);
  delay(1000);

  delay(2000);
}
void determineMaximumPayloadOfRobotArm() {
  Serial.println("Determine maximum payload of robot arm");
}
void determineMinimumICOfRobotArmUnderMP() {
  Serial.println("Determine minimum payload of robot arm");
}
void testMinimumAndMaximumWEOfRobotArm() {
  Serial.println("Test minimum and maximum work enveloper of robot arm");
}
void testEmergencyStop() {
  Serial.println("Test emergency stop");
}
void testErrorRecovery() {
  Serial.println("Test error recovery");
}

void load(uint8_t n = 0) {
  for (int i = 0; i < n; i++) {
    println("Move full rotation");
    singulator->moveFullRotation();
    delay(1000);
    println("Move to position A");
    robotArmControl->moveToPositionA();
    println("Close arm gripper");
    robotArmControl->armGripper.close();
    delay(1000);
    println("Move to position I1");
    robotArmControl->moveToPositionI1();
    println("Move to position B");
    robotArmControl->moveToPositionB();
    println("Open arm gripper");
    robotArmControl->armGripper.open();
    delay(1000);
    println("Move to position I2");
    robotArmControl->moveToPositionI2();
    if (i < (n-1)) {
      println("Next slot");
      carousel->nextSlot();
    }
  }
}

void sharpen(uint8_t n = 0) {
  for (int i = 0; i < n; i++) {  
    actuator->moveToPosition(2800);
    Serial.println("Close gripper");
    actuator->gripper->close();    
    delay(1000);
    Serial.println("Move to pre-sharpenning position");
    actuator->moveToPosition(1000);
    delay(1000);
    actuator->stepperMotor->setMaxSpeed(110.0);
    actuator->stepperMotor->setAcceleration(110.0);
    actuator->stepperMotor->setSpeed(110.0);
    delay(600);
    Serial.println("Move slowly while sharpenning");
    actuator->moveToPosition(200);
//    actuator->moveToHomePosition();
    delay(2000);
    actuator->stepperMotor->setMaxSpeed(300.0);
    actuator->stepperMotor->setAcceleration(300.0);
    actuator->stepperMotor->setSpeed(300.0);
    delay(600);
    Serial.println("Move back to pre-sharpenning position");
    actuator->moveToPosition(1000);
    Serial.println("Move to place position");
    actuator->moveToPosition(2500);
    Serial.println("Open gripper");
    actuator->gripper->open();
    
    delay(1000);
    actuator->moveToHomePosition();
    delay(2000);
    if (i < (n-1)) {
      println("Next slot");
      carousel->nextSlot();
      delay(3000);
    }
  }
}

void unload(uint8_t n = 0) {
  for (int i = 0; i < n; i++) {
    println("Move to position C");
    robotArmControl->moveToPositionC();
    println("Close arm gripper");
    robotArmControl->armGripper.close();
    delay(3000);
    println("Move to position I2_1");
    robotArmControl->moveToPositionI2_1();
    println("Move to position D");
    robotArmControl->moveToPositionD();
    println("Open arm gripper");
    robotArmControl->armGripper.open();
    delay(3000);
    println("Move to position I3");
    robotArmControl->moveToPositionI3();
    if (i < (n-1)) {
      println("Next slot");
      carousel->nextSlot();
      delay(3000);
    }
  }
}

void orderPencil() {
  int numberOfPencils = 0;
  int placeOrder = false;

  hmi->mainDisplay->clear();
  hmi->mainDisplay->setCursor(0,0);
  hmi->mainDisplay->print("Number of pencils:");
  hmi->mainDisplay->setCursor(0,1);
  hmi->mainDisplay->print(numberOfPencils);
  
  while (1) {
    if (hmi->upButton->isClicked()) {
      numberOfPencils++;
      hmi->mainDisplay->clear();
      hmi->mainDisplay->setCursor(0,0);
      hmi->mainDisplay->print("Number of pencils:");
      hmi->mainDisplay->setCursor(0,1);
      hmi->mainDisplay->print(numberOfPencils);
      Serial.print("Number of pencils:");
      Serial.println(numberOfPencils);
    } else if (hmi->downButton->isClicked()) {
      numberOfPencils--;
      hmi->mainDisplay->clear();
      hmi->mainDisplay->setCursor(0,0);
      hmi->mainDisplay->print("Number of pencils:");
      hmi->mainDisplay->setCursor(0,1);
      hmi->mainDisplay->print(numberOfPencils);
      Serial.print("Number of pencils:");
      Serial.println(numberOfPencils);
      if (numberOfPencils < 0) numberOfPencils = 0;
    } else if (hmi->selectButton->isClicked()) {                                                                                                                             
      placeOrder = true;
      break;
    } else if (hmi->backButton->isClicked()) {
      hmi->mainDisplay->clear();
      hmi->mainDisplay->setCursor(0,0);
      hmi->mainDisplay->print(hmi->menu->current->label);
      Serial.print("Number of pencils:");
      Serial.println(numberOfPencils);
      hmi->menu->back();
      break;
    }
  }

  if (placeOrder) {
    switch (numberOfPencils) { 
      case 1:
        load(1);
        carousel->nextSlot();
        delay(3000);
        carousel->nextSlot();
        delay(3000);
        sharpen(1);
        carousel->nextSlot();
        delay(3000);
        carousel->nextSlot();
        delay(3000);
        unload(1);
        break;
      case 2:
        load(2);
        carousel->nextSlot();
        delay(3000);
        sharpen(2);
        carousel->nextSlot();
        delay(3000);
        unload(2);
        break;
      case 3:
        load(3);
        sharpen(3);
        unload(3);
        break;
      case 4:
        load(3);
        sharpen(1);
        carousel->nextSlot();
        delay(3000);
        load(1);
        sharpen(1);
        carousel->nextSlot();
        delay(3000);
        sharpen(1);
        unload(1);
        carousel->nextSlot();
        delay(3000);
        sharpen(1);
        unload(3);
        break;
      case 5:
        load(5);
        sharpen(5);
        unload(5);
        break;
    }
  }
}

void pauseISR() {
  now = millis();
  if (abs(now - last) > 500) {  
    if (PAUSE == true) PAUSE = false;
    else if (PAUSE == false) PAUSE = true;
    
//    pauseRoutine();
    
    last = now;
  }
}

void pauseRoutine() {
    if (PAUSE == true) {
      actuator->setPause(true);
      carousel->setPause(true);
      singulator->setPause(true);
      robotArmControl->setPause(true);
      sharpener->setPause(true);     
    } else if (PAUSE == false) {
      actuator->setPause(false);
      carousel->setPause(false);
      singulator->setPause(false);
      robotArmControl->setPause(false);
      sharpener->setPause(false);
    }
}

void resetISR() {
  now = millis();
  if (abs(now - last) > 500) {  
    if (EMERGENCY == false && ESTOP_PREVIOUSLY_TRIGGERED == true) {
      resetArduino();
    }
    last = now;
  }
}

void emergencyISR() {
  now = millis();
  if (abs(now - last) > 500) {  
    if (EMERGENCY == true) EMERGENCY = false;
    else if (EMERGENCY == false) EMERGENCY = true;
    
    if (EMERGENCY == true) emergencyRoutine();
    
    last = now;
  }
}

void emergencyRoutine() {
    if (EMERGENCY == true) {
      ESTOP_PREVIOUSLY_TRIGGERED = true;
      actuator->setEmergency(true);
      carousel->setEmergency(true);
      singulator->setEmergency(true);
      robotArmControl->setEmergency(true);
      sharpener->setEmergency(true);     
    } 
}


void triggerStartSwitch() {

  hmi->statusDisplay->clear();
  hmi->statusDisplay->setCursor(0, 0);
  hmi->statusDisplay->print("Move to start");
  hmi->statusDisplay->setCursor(0, 1);
  hmi->statusDisplay->print("process");
  robotArmControl->moveToStartSwitch();
  delay(1000);
}

void moveToMaintanencePosition() {
  robotArmControl->moveToMaintanencePosition();
  delay(1000);
}
