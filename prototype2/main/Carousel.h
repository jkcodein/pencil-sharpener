/*
 * Parts:
 *   Stepper Motor
 *   Limit Switch
 *   Infrared Sensor
 */

#ifndef Carousel_h
#define Carousel_h

#include "Arduino.h"
#include "config.h"

#include "LimitSwitch.h"
#include "AccelStepper.h"
#include "InfraredSensor.h"


class Carousel {
  public:
    Carousel();
    Carousel(int homePosition_, int slot_);
    Carousel(int homePosition_, int maxPosition_, int direction_);

    AccelStepper* stepperMotor;
    InfraredSensor* infraredSensor;

    int homePosition;
    int maxPosition;
    int slot;
    int direction;

    boolean initialized = false;
    boolean emergency = false;
    boolean pause = false;

    void init();
    void moveToHomePosition();
    void moveToMaxPosition();
    void moveToPosition(int position_);
    void nextSlot();
    void setEmergency(boolean emergency_);
    void setPause(boolean pause_);
};

#endif
