#include "Emergency.h"

void pause(int (*func)()) {
  while(1) {
    if ((*func)()) {
      break;
    }
  }
}

EmergencyStopButton::EmergencyStopButton(uint8_t pin_, short interruptPin_) {
  this->pin = pin_;
  this->interruptPin = interruptPin_;
  pinMode(this->pin, INPUT_PULLUP);
}

boolean EmergencyStopButton::isPressed() {
  boolean val = digitalRead(this->pin);
  if (val == LOW) {
    this->state = this->PRESSED;
    return true;  
  }
  
  return false;
}

boolean EmergencyStopButton::isReleased() {
  return digitalRead(this->pin) == HIGH;
}

int EmergencyStopButton::read() {
  return digitalRead(this->pin);
}
