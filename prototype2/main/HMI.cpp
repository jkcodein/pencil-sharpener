#include "HMI.h"

HMI::HMI() {
  this->mode = 0;  
}

void HMI::init() {  
  this->mainDisplay->begin(16, 2);
  this->mainDisplay->clear();
  this->mainDisplay->setCursor(0, 0);
  this->mainDisplay->print("PencilBot v1.0");

  this->statusDisplay->begin(16, 2);
  this->statusDisplay->clear();
  this->statusDisplay->setCursor(0, 0);
  this->statusDisplay->print("Welcome");

  this->menu->init();
  this->menu->down();
}

void HMI::update() {
  this->mainDisplay->clear();
  this->mainDisplay->setCursor(0,0);
  this->mainDisplay->print(this->menu->current->label);
  Serial.print("Menu: ");
  Serial.println(this->menu->current->label);

  if (this->menu->selected != NULL) {
    this->statusDisplay->clear();
    this->statusDisplay->setCursor(0,0);
    this->statusDisplay->print("Selected:");
    this->statusDisplay->setCursor(0,1);
    this->statusDisplay->print(this->menu->selected->label);
    Serial.print("Selected: ");
    Serial.println(this->menu->selected->label);  
  }
}

short HMI::select() {
  short key = -1;
  while (1) {
    if (this->upButton->isClicked()) {
      this->menu->left();
      break;
    }

    if (this->downButton->isClicked()) {      
      this->menu->right();
      break;
    }

    if (this->selectButton->isClicked()) {
      this->menu->select();
      key = this->menu->selected->key;
      break;
    }

    if (this->backButton->isClicked()) {
      this->menu->back();
      break;
    }

    if (this->startButton->isReleased()) {
      key = 101;
      break;
    }
  }

  return key;
}

void Menu::init() {
  this->items[0].label = "Menu";
  this->items[1].label = "Main mode";
    this->items[4].label = "Order pencil";
  this->items[2].label = "Maintenance mode";
    this->items[5].label = "Run dry cycle";
    this->items[6].label = "Move to M pos";
    this->items[7].label = "Init system";
    this->items[8].label = "Init actuator";
    this->items[9].label = "Init carousel";
    this->items[10].label = "Init robot arm";
    this->items[11].label = "Init sharpener";
    this->items[12].label = "Init singulator";
    this->items[13].label = "Init HMI";
  this->items[3].label = "Test mode";
    this->items[14].label = "Test actuator";
    this->items[15].label = "Test carousel";
    this->items[16].label = "Test robot arm";
    this->items[17].label = "Test sharpener";
    this->items[18].label = "Test singulator";
    this->items[19].label = "Test RRA";
    this->items[20].label = "Determine MPRA";
    this->items[21].label = "Determine MIS";
    this->items[22].label = "Test MMWE";
    this->items[23].label = "Test E-stop";
    this->items[24].label = "Test ER";
    

  this->setFirst(&(this->items[1]), &(this->items[0]));
  this->setRight(&(this->items[2]), &(this->items[1]));
  this->setRight(&(this->items[3]), &(this->items[2]));

  this->setFirst(&(this->items[4]), &(this->items[1]));

  this->setFirst(&(this->items[5]), &(this->items[2]));
  this->setRight(&(this->items[6]), &(this->items[5]));
  this->setRight(&(this->items[7]), &(this->items[6]));
  this->setRight(&(this->items[8]), &(this->items[7]));
  this->setRight(&(this->items[9]), &(this->items[8]));
  this->setRight(&(this->items[10]), &(this->items[9]));
  this->setRight(&(this->items[11]), &(this->items[10]));
  this->setRight(&(this->items[12]), &(this->items[11]));
  this->setRight(&(this->items[13]), &(this->items[12]));

  this->setFirst(&(this->items[14]), &(this->items[3]));
  this->setRight(&(this->items[15]), &(this->items[14]));
  this->setRight(&(this->items[16]), &(this->items[15]));
  this->setRight(&(this->items[17]), &(this->items[16]));
  this->setRight(&(this->items[18]), &(this->items[17]));
  this->setRight(&(this->items[19]), &(this->items[18]));
  this->setRight(&(this->items[20]), &(this->items[19]));
  this->setRight(&(this->items[21]), &(this->items[20]));
  this->setRight(&(this->items[22]), &(this->items[21]));
  this->setRight(&(this->items[23]), &(this->items[22]));
  this->setRight(&(this->items[24]), &(this->items[23]));
  for (uint8_t k = 0; k < 25; k++) {
    this->items[k].key = k;
  }

  this->root = &(this->items[0]);
  this->root->left = this->root;
  this->root->right = this->root;
  this->root->parent = this->root;
  this->current = this->root;
}

void Menu::setFirst(Item* item, Item* rel) {
  rel->first = item;
  item->parent = rel;
  item->right = item;
  item->left = item;
}

void Menu::setRight(Item* item, Item* rel) {
  rel->right = item;
  item->left = rel;
  item->parent = rel->parent;
  item->right = item->parent->first;
  item->right->left = item;
}

void Menu::left() {
  this->current = this->current->left;
}

void Menu::right() {
  this->current = this->current->right;
}

void Menu::back() {
  this->current = this->current->parent;
}

void Menu::up() {
  
}

void Menu::down() {
  this->selected = this->current;
  Serial.print("Selected : ");
  Serial.println(this->current->label);
  if (this->current->first != NULL) this->current = this->current->first;
}

void Menu::set() {
  
}

void Menu::select() {
  this->selected = this->current;
  Serial.print("Selected : ");
  Serial.println(this->current->label);
  if (this->current->first != NULL) this->current = this->current->first;
  this->updateScreen = true;
}

void Menu::enter() {
  
}

void Menu::yes() {
  
}

void Menu::no() {
  
}
