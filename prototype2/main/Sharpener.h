#ifndef Sharpener_h
#define Sharpener_h

#include "Arduino.h"
#include "config.h"

class Sharpener {
  public:
    uint8_t pin;
    boolean initialized = false;
    boolean emergency = false;
    boolean pause = false;

    Sharpener(uint8_t pin_);
    
    void init();  
    void setEmergency(boolean emergency_);
    void triggerON();
    void triggerOFF();
    void setPause(boolean pause_);
};

#endif
