/*
 * Menu
 *   Run Mode
 *      Start
 *      Stop
 *      Reset
 *      Order
 *   Dry Mode
 *      Start
 *      Stop
 *      Reset
 *      Order
 *      Step
 *   Maintanence Mode
 *      Start
 *      Stop
 *      Reset
 *      Order
 *      Step
 *      Set Parameters
 *      Test   
 *        Singulator
 *          Step
 *          Home position
 *          Sense object
 *        Carousel
 *          Step
 *          Home position
 *          Sense object
 *        Actuator
 *          Home
 *          Pick position
 *          Pick up
 *          Sharpen position
 *          Hold for sharpener
 *          Place position
 *          Place
 *        Sharpener
 *          Sharpen
 *          Stop
 *        RobotArm
 *          Home position
 *          Pick position
 *          Pick up
 *          Place position
 *          Place
 *          
 * =================================================
 * Run Mode is a main operating mode of pencil sharpener machine.
 * In this mode, a user can order any number of pencils what he or she wants to.
 * There are Set, Back and Numbers buttons to order pencils.
 * Firstly, a user should enter a number of pencils. 
 * Secondly, the user should select set button and confirm the number of pencils.
 * =================================================
 * Dry mode is a slow operating mode of pencil sharpener machine.
 * In this mode, the machine runs on 25% of its actual speed.
 * 
 */
