#include "Sharpener.h"

Sharpener::Sharpener(uint8_t pin_) {
  this->pin = pin_;
  pinMode(this->pin, OUTPUT);
}

void Sharpener::init() {
  digitalWrite(this->pin, LOW);
  delay(500);
}

void Sharpener::triggerON() {
  digitalWrite(this->pin, HIGH);
  delay(500);
}

void Sharpener::triggerOFF() {
  digitalWrite(this->pin, LOW);
  delay(500);
}

void Sharpener::setEmergency(boolean emergency_) {
  this->emergency = emergency_;
  if (this->emergency) {
    this->triggerON();
    this->triggerOFF();
    this->triggerON();
  }
}

void Sharpener::setPause(boolean pause_) {
  this->pause = pause_;
  if (this->pause) {
    this->triggerON();
  } else {
    this->triggerOFF();
  }
};
