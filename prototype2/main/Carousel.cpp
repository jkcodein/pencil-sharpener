#include "Carousel.h"

Carousel::Carousel() {
  this->homePosition = 0;
}

Carousel::Carousel(int homePosition_, int slot_) {
  this->homePosition = homePosition_;
  this->slot = slot_;
}


Carousel::Carousel(int homePosition_, int maxPosition_, int direction_) {
  this->homePosition = homePosition_;
  this->maxPosition = maxPosition_;
  this->direction = direction_;
}

void Carousel::init() {
  this->moveToHomePosition();
  this->initialized = true;
}

void Carousel::moveToHomePosition() {
  
  long i = -1; // Set opposite direction
  boolean extraMovementToAlign = false;
  this->stepperMotor->moveTo(i);
  
  if ( ! this->infraredSensor->isObject() ) {
    extraMovementToAlign = true;
  }
  
  while ( ! this->infraredSensor->isObject() ) {
    if (this->emergency) return;
    while (this->pause); 
    this->stepperMotor->runSpeed();    
  }

  if (extraMovementToAlign) {
    this->moveToPosition(this->stepperMotor->currentPosition() - 4);
  }
  
  this->stepperMotor->setCurrentPosition(this->homePosition);
}

void Carousel::moveToMaxPosition() {
  this->stepperMotor->moveTo(this->maxPosition);
  while(this->stepperMotor->distanceToGo() != 0) {
    if (this->emergency) return;
    while (this->pause);
    this->stepperMotor->run();
    delay(0);
  }
}

void Carousel::moveToPosition(int position_) {
  this->stepperMotor->moveTo(position_);
  while(this->stepperMotor->distanceToGo() != 0) {
    if (this->emergency) return;
    while (this->pause);
    this->stepperMotor->run();
    delay(0);
  }
}

void Carousel::nextSlot() {
  int nextPosition = this->stepperMotor->currentPosition() + this->slot;
  this->stepperMotor->moveTo(nextPosition);
  while(this->stepperMotor->distanceToGo() != 0) {
    if (this->emergency) return;
    while (this->pause);
    this->stepperMotor->run();
    delay(1);
  }
  
  if (this->infraredSensor->isObject()) { // && this->stepperMotor->currentPosition() % 5 == 0
    this->stepperMotor->setCurrentPosition(0);
  }
}

void Carousel::setEmergency(boolean emergency_) {
  this->emergency = emergency_;
}

void Carousel::setPause(boolean pause_) {
  this->pause = pause_;
}
