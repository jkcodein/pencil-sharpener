#include <Wire.h>
#include <LiquidCrystal_I2C.h>  //POB $A
//LiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2); // ARDUINO UNO

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

struct button {
  uint8_t pinNumber;
  String label = String(20);
};

class Item {  
  public:
    String label;
    uint8_t key;
    Item *parent;
    Item *left;
    Item *right;
    Item *first;
    
    Item() {
      label = "";
      this->parent = NULL;
      this->left = NULL;
      this->right = NULL;
      this->first = NULL;
    };

    Item(String _label) {
      this->label = _label;
      this->parent = NULL;
      this->left = NULL;
      this->right = NULL;
      this->first = NULL;
    };
};

class Menu {
  public:
    Item *root;
    Item *current;
    button buttons[4];
    LiquidCrystal_I2C *lcd;
    uint8_t selected;
    boolean updateScreen = true;

    Menu() {
      this->root = NULL;
      this->current = NULL;
    };

    void left() {
      this->current = this->current->left;
    }

    void right() {
      this->current = this->current->right;
    }

    void back() {
      this->current = this->current->parent;
    }

    void up() {
      
    }

    void down() {
      
    }

    void set() {
      this->selected = this->current->key;
      this->updateScreen = true;
    }

    void select() {
      this->current = this->current->first;
    }

    void enter() {
      
    }

    void yes() {
      
    }

    void no() {
      
    }
};

Item items[7 + 1];
Menu menu;

void setup() {
  Serial.begin(9600);
  
  items[0].label = "Menu";
  items[1].label = "Order pencils";
  items[2].label = "Maintenance mode";
    items[4].label = "Demo mode";
    items[5].label = "25% Speed";
      items[7].label = "Order pencils";
    items[6].label = "Dry run";
  items[3].label = "Pencil loading";  
  
  items[0].first = &(items[1]);
  items[0].right = &(items[0]);
  items[0].left = &(items[0]);
  
  items[1].parent = &(items[0]);
  items[1].right = &(items[2]);
  items[1].right->left = &(items[1]);
  items[1].left = &(items[3]);
  
  items[2].parent = &(items[0]);
  items[2].right = &(items[3]);
  items[2].right->left = &(items[2]);
  
  items[3].parent = &(items[0]);
  items[3].right = &(items[1]);

  items[2].first = &(items[4]);
  items[4].parent = &(items[2]);
  items[4].right = &(items[5]);
  items[4].right->left = &(items[4]);
  items[4].left = &(items[6]);

  items[5].parent = &(items[2]);
  items[5].right = &(items[6]);
  items[5].right->left = &(items[5]);

  items[6].parent = &(items[2]);
  items[6].right = &(items[4]);

  items[5].first = &(items[7]);
  items[7].parent = &(items[5]);
  items[7].left = &(items[7]);
  items[7].right = &(items[7]);
  
  for (uint8_t k = 0; k < 8; k++) {
    items[k].key = k;
  }

  menu.root = &(items[0]);
  menu.current = menu.root;

  menu.buttons[0].pinNumber = 30;
  menu.buttons[0].label = "<";

  menu.buttons[1].pinNumber = 31;
  menu.buttons[1].label = ">";

  menu.buttons[2].pinNumber = 32;
  menu.buttons[2].label = "o";

  menu.buttons[3].pinNumber = 33;
  menu.buttons[3].label = "^";

  for (uint8_t i = 0; i < 4; i++) {
    pinMode(menu.buttons[i].pinNumber, INPUT_PULLUP); 
  }
  
  menu.lcd = &lcd;
  menu.lcd->begin(16,2); 
  menu.lcd->clear();
  menu.lcd->setCursor(0,0); 
  menu.lcd->print("Welcome!");
  menu.lcd->setCursor(0,1);
  menu.lcd->print("Pencil Sharpener v1.");  
  
  delay(1000);
}

void loop() {
  switch(menu.selected) {
    case 1:
      handleOrderPencils();
      break;
    case 3:
      handlePencilLoading();
      break;
    case 4:
      handleDemoMode();
      break;
    case 6:
      handleDryMode();
      break;
    case 7:
      handle25ModeOrderPencils();
      break;  
    default:
      handleDefault();
  }
}

int count = 0, state = 0;
int count1 = 0, state1 = 0;

void handleOrderPencils() { 
  if (state == 0) {
    if (menu.updateScreen) {
      menu.updateScreen = false;
      menu.lcd->clear();
      menu.lcd->setCursor(0,0);
      Serial.println("Enter 1-8"); 
      menu.lcd->print("Enter 1-8");
      menu.lcd->setCursor(0,1);
      menu.lcd->print(count);
      delay(100);  
    }
        
    if (digitalRead(menu.buttons[0].pinNumber) == LOW) {
      if (count > 0) count--;
      menu.updateScreen = true;
      delay(700);
    } 
  
    if (digitalRead(menu.buttons[1].pinNumber) == LOW) {
      if (count < 8) count++;
      menu.updateScreen = true;
      delay(700);
    }

    if (digitalRead(menu.buttons[2].pinNumber) == LOW) {
      menu.lcd->clear();
      menu.lcd->setCursor(0,0);
      menu.lcd->print("Confirm Y/N");
      state = 1;
      delay(700);
    } 

    if (digitalRead(menu.buttons[3].pinNumber) == LOW) {
      menu.updateScreen = true;
      menu.current = menu.root;
      delay(700);
    }    
  }

  if (state == 1) {
    if (digitalRead(menu.buttons[2].pinNumber) == LOW) {
      menu.lcd->clear();
      menu.lcd->setCursor(0,0);
      menu.lcd->print("Sharpenning");
      int remainingTime = 20;
      while(remainingTime-- > 0) {
        menu.lcd->setCursor(0,1);
        menu.lcd->print(remainingTime);
        menu.lcd->print(" ");
        delay(1000);
      }
      state = 0;
      count = 0;
      menu.updateScreen = true;
      menu.current = menu.root;
      menu.selected = 0;
      delay(700);
    } 
  
    if (digitalRead(menu.buttons[3].pinNumber) == LOW) {
      menu.updateScreen = true;
      menu.current = menu.root;
      delay(700);
    }  
  }
}

void handlePencilLoading() {
  if (state == 0) {
    if (menu.updateScreen) {
      menu.updateScreen = false;
      menu.lcd->clear();
      menu.lcd->setCursor(0,0);
      Serial.println("Enter number 1-8"); 
      menu.lcd->print("Enter number 1-8");
      menu.lcd->setCursor(0,1);
      menu.lcd->print(count);
      delay(100);  
    }
        
    if (digitalRead(menu.buttons[0].pinNumber) == LOW) {
      if (count > 0) count--;
      menu.updateScreen = true;
      delay(700);
    } 
  
    if (digitalRead(menu.buttons[1].pinNumber) == LOW) {
      if (count < 8) count++;
      menu.updateScreen = true;
      delay(700);
    }

    if (digitalRead(menu.buttons[2].pinNumber) == LOW) {
      menu.lcd->clear();
      menu.lcd->setCursor(0,0);
      menu.lcd->print("Confirm Y/N");
      state = 1;
      delay(700);
    } 

    if (digitalRead(menu.buttons[3].pinNumber) == LOW) {
      menu.updateScreen = true;
      menu.current = menu.root;
      delay(700);
    }    
  }

  if (state == 1) {
    if (digitalRead(menu.buttons[2].pinNumber) == LOW) {
      menu.lcd->clear();
      menu.lcd->setCursor(0,0);
      menu.lcd->print("Loading");
      int remainingTime = 15;
      while(remainingTime-- > 0) {
        menu.lcd->setCursor(0,1);
        menu.lcd->print(remainingTime);
        menu.lcd->print(" ");
        delay(1000);
      }
      state = 0;
      count = 0;
      menu.updateScreen = true;
      menu.current = menu.root;
      menu.selected = 0;
      delay(700);
    } 
  
    if (digitalRead(menu.buttons[3].pinNumber) == LOW) {
      menu.updateScreen = true;
      menu.current = menu.root;
      delay(700);
    }  
  }
}

void handleDemoMode() {
  menu.lcd->clear();
  menu.lcd->setCursor(0,0);
  menu.lcd->print("Demo Mode");
  int remainingTime = 10;
  while(remainingTime-- > 0) {
    menu.lcd->setCursor(0,1);
    menu.lcd->print(remainingTime);
    menu.lcd->print(" ");
    delay(1000);
  }
  state = 0;
  count = 0;
  menu.updateScreen = true;
  menu.current = menu.root;
  menu.selected = 0;
  delay(700);
}

void handleDryMode() {
  menu.lcd->clear();
  menu.lcd->setCursor(0,0);
  menu.lcd->print("Dry Mode");
  int remainingTime = 10;
  while(remainingTime-- > 0) {
    menu.lcd->setCursor(0,1);
    menu.lcd->print(remainingTime);
    menu.lcd->print(" ");
    delay(1000);
  }
  state = 0;
  count = 0;
  menu.updateScreen = true;
  menu.current = menu.root;
  menu.selected = 0;
  delay(700);  
}

void handle25ModeOrderPencils() {
  handleOrderPencils();
}

void handleDefault() {
  if (menu.updateScreen) {
    menu.updateScreen = false;
    menu.lcd->clear();
    menu.lcd->setCursor(0,0);
    Serial.println(menu.current->label); 
    menu.lcd->print(menu.current->label);
    delay(100);  
  }
  
  if (digitalRead(menu.buttons[0].pinNumber) == LOW) {
    menu.left();
    menu.updateScreen = true;
    delay(700);
  } 

  if (digitalRead(menu.buttons[1].pinNumber) == LOW) {
    menu.right();
    menu.updateScreen = true;
    delay(700);
  } 

  if (digitalRead(menu.buttons[2].pinNumber) == LOW) {
    if (menu.current->first != NULL) {
      menu.select();
      menu.updateScreen = true;
    } else {
      menu.set();
    }
    delay(700);
  } 

  if (digitalRead(menu.buttons[3].pinNumber) == LOW) {
    if (menu.current->parent != NULL) {
      menu.back();
      menu.updateScreen = true;
      delay(700);
    }
  }
}
