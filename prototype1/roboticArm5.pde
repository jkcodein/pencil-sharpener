import processing.serial.*;

Serial serialPort;  // Create object from Serial class
String val;     // Data received from the serial port
PGraphics s0, s1, s2, s3, s4, s5;

void setup()
{
  size(1024,600);
  background(245);
  String portName = Serial.list()[0]; //change the 0 to a 1 or 2 etc. to match your port
  println(portName);
  serialPort = new Serial(this, portName, 9600);
}

String data;
String[] angleOfServos = {"90", "90", "90", "90", "90", "90", "90"};
int[] angles = {90,90,90,90,90,90,90};
void draw()
{
  if ( serialPort.available() > 0) { // If data is available,
    data = serialPort.readStringUntil('\n'); // read it and store it in val
    println(data);
    if (data != null && data.length() > 30) {
      angleOfServos = data.split(",");
      for (int i = 0; i < 6; i++) {
        angles[i] = Integer.parseInt(angleOfServos[i]);
      }
    }
  } 
  
  angleComponent(0*160 + 100, 80, "Base", angles[0]);
  angleComponent(1*160 + 100, 80, "Shoulder", angles[1]);
  angleComponent(2*160 + 100, 80, "Upper Arm", angles[2]);
  angleComponent(3*160 + 100, 80, "Lower Arm", angles[3]);
  angleComponent(4*160 + 100, 80, "Wrist", angles[4]);
  angleComponent(5*160 + 100, 80, "Gripper", angles[5]);
  gridComponent(250, 350, 20, 20, 10, "X", "Y");
  gridComponent(750, 350, 20, 20, 10, "Y", "Z");
  
  baseLineDirectionComponent(250, 350, angles[0]);
  shoulderPositionComponentXY(250, 350, angles[1], angles[0]);
  shoulderPositionComponentYZ(750, 350, angles[1], angles[0]);
  
  upperArmPositionComponentXY(250, 350, angles[2], angles[1], angles[0]);
  upperArmPositionComponentYZ(750, 350, angles[2], angles[1], angles[0]);
  
  lowerArmPositionComponentXY(250, 350, angles[3], angles[2], angles[1], angles[0]);
  lowerArmPositionComponentYZ(750, 350, angles[3], angles[2], angles[1], angles[0]);
  
  buttonComponent("Set", 480, 170);
}

void mouseClicked() {
  if (sqrt(pow(mouseX - 100 - 0 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[0] = (int)(acos((mouseX - 100 - 0 * 160) / sqrt(pow(mouseX - 100 - 0 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 1 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[1] = (int)(acos((mouseX - 100 - 1 * 160) / sqrt(pow(mouseX - 100 - 1 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 2 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[2] = (int)(acos((mouseX - 100 - 2 * 160) / sqrt(pow(mouseX - 100 - 2 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 3 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[3] = (int)(acos((mouseX - 100 - 3 * 160) / sqrt(pow(mouseX - 100 - 3 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 4 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[4] = (int)(acos((mouseX - 100 - 4 * 160) / sqrt(pow(mouseX - 100 - 4 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 5 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[5] = (int)(acos((mouseX - 100 - 5 * 160) / sqrt(pow(mouseX - 100 - 5 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } 
  
  if (abs(mouseX - 480) < 50 && abs(mouseY - 170) < 30) {
    String instruction = "";
    for (int i = 0; i < 6; i++) {
      instruction += angles[i];
      if (i < 5) instruction += ","; 
    }
    instruction += "\n";
    serialPort.write(instruction);
  }
}

void mouseDragged() {
  clear();
  redraw();
  background(245);
  if (sqrt(pow(mouseX - 100 - 0 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[0] = (int)(acos((mouseX - 100 - 0 * 160) / sqrt(pow(mouseX - 100 - 0 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 1 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[1] = (int)(acos((mouseX - 100 - 1 * 160) / sqrt(pow(mouseX - 100 - 1 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 2 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[2] = (int)(acos((mouseX - 100 - 2 * 160) / sqrt(pow(mouseX - 100 - 2 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 3 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[3] = (int)(acos((mouseX - 100 - 3 * 160) / sqrt(pow(mouseX - 100 - 3 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 4 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[4] = (int)(acos((mouseX - 100 - 4 * 160) / sqrt(pow(mouseX - 100 - 4 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  } else if (sqrt(pow(mouseX - 100 - 5 * 160, 2) + pow(mouseY - 80,2)) < 50 && (-(mouseY - 80) > 0) ) {
    angles[5] = (int)(acos((mouseX - 100 - 5 * 160) / sqrt(pow(mouseX - 100 - 5 * 160,2) + pow(mouseY - 80,2))) * 180/PI); 
  }
}

void mouseReleased() {
   clear();
   redraw();
   background(245);
}

void buttonComponent(String label, int cx, int cy) {
  fill(255);
  rect(cx, cy, 50, 30);
  fill(0);
  text(label, cx + 25, cy + 12);
}

void gridComponent(int cx, int cy, int sx, int sy, int n, String labelAxis1, String labelAxis2) {
  stroke(200);
  for (int i = -n; i <= n; i++) {
    line(cx + (-n) * sx, cy + i * sx, cx + n * sx, cy + i * sx);
    line(cx + i * sy, cy + (-n) * sy, cx + i * sy, cy + n * sy);
  }
  stroke(0);
  line(cx + (-n) * sx, cy, cx + n * sx, cy);
  line(cx, cy + (-n) * sy, cx, cy + n * sy);
  textAlign(CENTER, CENTER);
  text(labelAxis1, cx + (n + 1) * sx, cy);
  text(labelAxis2, cx, cy + (-n - 1) * sy);
}

void angleComponent(int cx, int cy, String label, int angle) {
  noFill();
  stroke(175);
  arc(cx, cy, 100, 100, PI, 2 * PI);
  fill(255, 0, 0);
  stroke(255, 0, 0);
  ellipse(cx + 50 * cos(angle * PI / 180), cy - 50 * sin(angle * PI / 180), 5, 5);
  noFill();
  stroke(0,0,0);
  fill(0);
  textAlign(CENTER);
  textSize(20);
  text(angle, cx, cy);
  textSize(16);
  fill(100);
  text(label, cx, cy + 30);
  fill(0);
}

void baseLineDirectionComponent(int cx, int cy, int angle) {
  line(cx - 10 * cos(angle * PI/180), cy + 10 * sin(angle * PI/180), cx + 10 * cos(angle * PI/180), cy - 10 * sin(angle * PI/180));
}

void shoulderPositionComponentXY(int cx, int cy, int angle, int baseAngle) {
   float r = 52.5 * cos(angle * PI/180);
   stroke(255,0,0);
   line(cx, cy, 
        cx + r*cos(baseAngle * PI/180), 
        cy - r*sin(baseAngle * PI/180)); 
   stroke(0);
}

void shoulderPositionComponentYZ(int cx, int cy, int angle, int baseAngle) {
   stroke(255,0,0);
   line(cx, cy, 
        cx + 52.5*cos(angle * PI/180) * sin(baseAngle * PI/180), 
        cy - 52.5*sin(angle * PI/180)); 
   stroke(0);
}

void upperArmPositionComponentXY(int cx, int cy, int angle, int shoulderAngle, int baseAngle) {
  float s = 52.5 * cos(shoulderAngle * PI/180);
   stroke(0,255,0);
   line(cx + s * cos(baseAngle * PI/180), 
        cy - s * sin(baseAngle * PI/180), 
        cx + s * cos(baseAngle * PI/180) + 75 * cos((angle + shoulderAngle - 90) * PI/180) * cos(baseAngle * PI/180), 
        cy - s * sin(baseAngle * PI/180) - 75 * cos((angle + shoulderAngle - 90) * PI/180) * sin(baseAngle * PI/180)); 
   stroke(0);
}

void upperArmPositionComponentYZ(int cx, int cy, int angle, int shoulderAngle, int baseAngle) {
   float sX = 52.5 * cos(shoulderAngle * PI/180) * sin(baseAngle * PI/180);
   float sY = 52.5 * sin(shoulderAngle * PI/180);
   stroke(0,255,0);
   line(cx + sX, 
        cy - sY, 
        cx + sX + 75*cos((angle + shoulderAngle - 90) * PI/180) * sin(baseAngle * PI/180), 
        cy - sY - 75*sin((angle + shoulderAngle - 90) * PI/180)); 
   stroke(0);
}

void lowerArmPositionComponentXY(int cx, int cy, int angle, int upperArmAngle, int shoulderAngle, int baseAngle) {
   float s1X = 52.5 * cos(shoulderAngle * PI/180) * cos(baseAngle * PI/180) + 75 * cos((upperArmAngle + shoulderAngle - 90) * PI/180) * cos(baseAngle * PI/180);
   float s1Y = 52.5 * cos(shoulderAngle * PI/180) * sin(baseAngle * PI/180) + 75 * cos((upperArmAngle + shoulderAngle - 90) * PI/180) * sin(baseAngle * PI/180);
   stroke(0,0,255);
   line(cx + s1X, 
        cy - s1Y, 
        cx + s1X + 100 * cos((angle + upperArmAngle + shoulderAngle - 180) * PI/180) * cos(baseAngle * PI/180), 
        cy - s1Y - 100 * cos((angle + upperArmAngle + shoulderAngle - 180) * PI/180) * sin(baseAngle * PI/180)); 
   stroke(0);
}

void lowerArmPositionComponentYZ(int cx, int cy, int angle, int upperArmAngle, int shoulderAngle, int baseAngle) {
   float sX = 52.5 * cos(shoulderAngle * PI/180) * sin(baseAngle * PI/180) + 75*cos((upperArmAngle + shoulderAngle - 90) * PI/180) * sin(baseAngle * PI/180);
   float sY = 52.5 * sin(shoulderAngle * PI/180) + 75*sin((upperArmAngle + shoulderAngle - 90) * PI/180);
   stroke(0,0,255);
   line(cx + sX, 
        cy - sY, 
        cx + sX + 100*cos((angle + upperArmAngle + shoulderAngle - 180) * PI/180) * sin(baseAngle * PI/180), 
        cy - sY - 100*sin((angle + upperArmAngle + shoulderAngle - 180) * PI/180)); 
   stroke(0);
}