/*
 * Parts
 *    6 x Servo Motor
 *    16 Channel PCA Board
 * Conditions & Requirements
 * 
 */

#ifndef RobotArm_h
#define RobotArm_h

#include "Arduino.h"
#include "config.h"
#include "LimitSwitch.h"
#include <Adafruit_PWMServoDriver.h>
//#include <Geometry.h>

struct PCABoardChannel {
  uint8_t number;
  uint16_t defaultPulseWidth;  
  uint16_t pulseWidth;
  uint16_t minPulseWidth;
  uint16_t maxPulseWidth;
};

class RobotArm {
  protected:
    String name = "RobotArm";
  public:
    Adafruit_PWMServoDriver* pwmDriver;
    PCABoardChannel channel;
    uint16_t minAngle;
    uint16_t maxAngle;
    uint16_t angle;
    uint16_t stepDelay;
    uint16_t defaultAngle;
    boolean emergency = false;
    boolean pause = false;
  
    RobotArm();

    void setAngle(uint16_t degree);
    void setDefaultPulseWidth(uint16_t pulseWidth);
    void sendPulseWidth(uint16_t pulseWidth); 
    void stepToDefaultAngle();
    void stepToAngle(uint16_t degree);
    void moveToDefaultAngle();
    void moveToAngle(uint16_t degree);
    void setEmergency(boolean emergency_);
    void setPause(boolean pause_);
    
};

class Base : public RobotArm { // inherit properties and methods of the RobotArm class
  public: 
    Base();
};

class Shoulder : public RobotArm {
  public:
    Shoulder();
};

class UpperArm : public RobotArm {
  public:
    UpperArm();
};

class LowerArm : public RobotArm {
  public:
    LowerArm();
};

class Wrist : public RobotArm {
  public:
    Wrist();
};

class ArmGripper : public RobotArm {
  public:
    ArmGripper();
    void open();
    void close();
};

class RobotArmControl {
  public:
    Base base;         
    Shoulder shoulder;
    UpperArm upperArm;
    LowerArm lowerArm;
    Wrist wrist;
    ArmGripper armGripper;
    LimitSwitch* startSwitch;
    uint8_t order[6];
    boolean start = false;
    boolean initialized = false;
    boolean emergency = false;
    boolean pause = false;

    RobotArmControl();
    void init();

    void moveTo(int x, int y, int z);
    void moveTo(short a1, short a2, short a3, short a4, short a5, short a6);   
    void moveToPositionA();
    void moveToPositionI1();
    void moveToPositionB();
    void moveToPositionI2();
    void moveToPositionC();
    void moveToPositionI2_1();
    void moveToPositionD();
    void moveToPositionI3();
    void setEmergency(boolean emergency_);
    void setPause(boolean pause_);
    void moveToStartSwitch();
    void moveToMaintanencePosition();
};

#endif
