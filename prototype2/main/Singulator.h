/*
 * Parts
 *   Stepper Motor
 *   Infrared Sensor
 *   ? Limit Switch
 */

#ifndef Singulator_h
#define Singulator_h

#include "Arduino.h"
#include "config.h"
#include "AccelStepper.h"
#include "InfraredSensor.h"

class Singulator {
  public:
    int homePosition;
    int fullRotation;
    boolean initialized = false;
    boolean emergency = false;
    boolean pause = false;

    AccelStepper* stepperMotor;
    InfraredSensor* infraredSensor1;
    InfraredSensor* infraredSensor2;
    
    Singulator();
    Singulator(int homePosition_, int fullRotation_);

    void init();  
    void moveToHomePosition();
    void moveFullRotation();
    void moveToPosition(int position_);
    void setEmergency(boolean emergency_);
    void setPause(boolean pause_);
};

#endif
