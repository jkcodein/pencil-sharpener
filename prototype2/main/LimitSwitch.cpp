#include "LimitSwitch.h"

LimitSwitch::LimitSwitch() {}

LimitSwitch::LimitSwitch(uint8_t pin_) {
  this->pin = pin_;
  pinMode(this->pin, INPUT_PULLUP);
}

boolean LimitSwitch::isOpen() {
//  if (DEBUG) {
//    return false; 
//  } 

  return digitalRead(this->pin) == HIGH;
}

boolean LimitSwitch::isClose() {
//  if (DEBUG) {
//    return true;
//  }
  
  return digitalRead(this->pin) == LOW;
}
