/*
 * Parts:
 *   Stepper Motor
 *   Limit Switch
 *   Servo Motor
 */
#ifndef Actuator_h
#define Actuator_h

#include "Arduino.h"
#include "config.h"

#include "LimitSwitch.h"
#include "AccelStepper.h"
#include "Gripper.h"

class Actuator {
  public:
    const uint8_t CW = 1;
    const uint8_t CCW = 2;
    boolean initialized = false;
    boolean emergency = false;
    boolean pause = false;

    LimitSwitch* homeLimitSwitch;
    AccelStepper* stepperMotor;
    Gripper* gripper;
    
    int homePosition;
    int maxPosition;
    
    Actuator();
    Actuator(int homePosition_, int maxPosition_);

    void init();
    void moveToHomePosition();
    void moveToMaxPosition();
    void moveToPosition(int position_);
    void setEmergency(boolean emergency_);
    void setPause(boolean pause_);
};

#endif
