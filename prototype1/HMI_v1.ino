#include <Wire.h>
#include <LiquidCrystal_I2C.h>  //POB $A
//LiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2); // ARDUINO UNO

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

struct button {
  uint8_t pinNumber;
  String label = String(20);
} buttons[4];

class Item {  
  public:
    String label;
    Item * parent;
    Item * left;
    Item * right;
    Item * first;
    
    Item() {
      label = "";
      this->parent = NULL;
      this->left = NULL;
      this->right = NULL;
      this->first = NULL;
    };

    Item(String _label) {
      this->label = _label;
      this->parent = NULL;
      this->left = NULL;
      this->right = NULL;
      this->first = NULL;
    };
};

class Menu {
  public:
    Item * root;
    Item * current;

    Menu() {
      this->root = NULL;
      this->current = NULL;
    };

    void up() {
      
    }

    void down() {
      
    }

    void back() {
      
    }

    void set() {
      
    }
};

Item items[15 + 1];
Menu menu;

void setup() {
  Serial.begin(9600);
  lcd.begin(16,2); 
  lcd.clear();

  buttons[0].pinNumber = 30;
  buttons[0].label = "Up";

  buttons[1].pinNumber = 31;
  buttons[1].label = "Down";

  buttons[2].pinNumber = 32;
  buttons[2].label = "Set";

  buttons[3].pinNumber = 33;
  buttons[3].label = "Back";
  
  for (uint8_t i = 0; i < 4; i++) {
    pinMode(buttons[i].pinNumber, INPUT_PULLUP); 
  }  

  items[0].label = "Menu";
  items[1].label = "Order pencils";
  items[2].label = "Maintenance mode";
  items[3].label = "Pencil loading";
  items[4].label = "Enter number 1-8";
  items[5].label = "Confirm Y/N";
  items[6].label = "Sharpenning";
  items[7].label = "Demo mode";
  items[8].label = "25% Speed";
  items[9].label = "Order pencils";
  items[10].label = "1-8";
  items[11].label = "Confirm Y/N?";
  items[12].label = "Sharpenning";
  items[13].label = "Dry run";
  items[14].label = "How many pencils loaded";
  items[15].label = "Confirm Y/N?";
  
  items[0].first = &(items[1]);
  items[0].right = &(items[0]);
  items[0].left = &(items[0]);
  
  items[1].parent = &(items[0]);
  items[1].right = &(items[2]);
  items[1].right->left = &(items[1]);
  items[1].left = &(items[3]);
  items[2].parent = &(items[0]);
  items[2].right = &(items[3]);
  items[2].right->left = &(items[2]);
  items[3].parent = &(items[0]);
  items[3].right = &(items[1]);

  items[1].first = &(items[4]);
  items[4].parent = &(items[1]);
  items[4].right = &(items[4]);
  items[4].left = &(items[4]);
  items[4].first = &(items[5]);
  items[5].parent = &(items[4]);
  items[5].right = &(items[5]);
  items[5].left = &(items[5]);
  items[5].first = &(items[6]);
  items[6].parent = &(items[5]);
  items[6].right = &(items[6]);
  items[6].left = &(items[6]);

  items[2].first = &(items[7]);
  items[7].parent = &(items[2]);
  items[7].right = &(items[8]);
  items[7].right->left = &(items[7]);
  items[7].left = &(items[13]);
  items[13].right = &(items[7]);
  items[8].parent = &(items[2]);
  items[8].right = &(items[13]);
  items[8].right->left = &(items[8]);
  items[8].first = &(items[9]);
  items[9].parent = &(items[8]);
  items[9].right = &(items[9]);
  items[9].left = &(items[9]);
  items[9].first = &(items[10]);
  items[10].parent = &(items[9]);
  items[10].right = &(items[10]);
  items[10].left = &(items[10]);
  items[10].first = &(items[11]);
  items[11].parent = &(items[10]);
  items[11].right = &(items[11]);
  items[11].left = &(items[11]);
  items[11].first = &(items[12]);
  items[12].parent = &(items[11]);
  items[12].right = &(items[12]);
  items[12].left = &(items[12]);
  
  items[3].first = &(items[14]);
  items[14].parent = &(items[3]);
  items[14].right = &(items[14]);
  items[14].left = &(items[14]);
  items[14].first = &(items[15]);
  items[15].parent = &(items[14]);
  items[15].right = &(items[15]);
  items[15].left = &(items[15]);  

  menu.root = &(items[0]);
  menu.current = menu.root;

  lcd.setCursor(0,0); 
  lcd.print("Welcome!");
  lcd.setCursor(0,1);
  lcd.print("Pencil Bot v1.");  
  
  delay(1000);
}

boolean updateScreen = true;

void loop() {
  if (digitalRead(buttons[0].pinNumber) == LOW) {
    menu.current = menu.current->left;
    updateScreen = true;
    delay(700);
  } 

  if (digitalRead(buttons[1].pinNumber) == LOW) {
    menu.current = menu.current->right;
    updateScreen = true;
    delay(700);
  } 

  if (digitalRead(buttons[2].pinNumber) == LOW) {
    if (menu.current->first != NULL) {
      menu.current = menu.current->first;
      updateScreen = true;  
      delay(700);
    }
  } 

  if (digitalRead(buttons[3].pinNumber) == LOW) {
    if (menu.current->parent != NULL) {
      menu.current = menu.current->parent;
      updateScreen = true;
      delay(700);
    }
  } 

  if (updateScreen) {
    updateScreen = false;
    lcd.clear();
    lcd.setCursor(0,0);
    Serial.println(menu.current->label); 
    lcd.print(menu.current->label);
    delay(100);  
  }
  
}
