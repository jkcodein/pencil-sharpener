#include "Gripper.h"

Gripper::Gripper() {}

Gripper::Gripper(uint8_t pin_, short minAngle_, short maxAngle_, short minPulseWidth_, short maxPulseWidth_) {
  this->pin = pin_;
  this->minAngle = minAngle_;
  this->maxAngle = maxAngle_;
  this->minPulseWidth = minPulseWidth_;
  this->maxPulseWidth = maxPulseWidth_;
}

void Gripper::open() {
  this->moveTo(this->minAngle);
}

void Gripper::close() {
  this->moveTo(this->maxAngle);
}

void Gripper::setAngle(short angle_) {
  this->angle = angle_;
  this->pulseWidth = map(this->angle, this->minAngle, this->maxAngle, this->minPulseWidth, this->maxPulseWidth);
}

void Gripper::moveTo(short angle_) {
  this->setAngle(angle_);
  if (this->emergency) return;
  while (this->pause);
  this->pwmDriver->setPWM(this->pin, 0, this->pulseWidth);
}

void Gripper::move() {
  if (this->emergency) return;
  while (this->pause);
  this->pwmDriver->setPWM(this->pin, 0, this->pulseWidth);
}

void Gripper::setEmergency(boolean emergency_) {
  this->emergency = emergency_;
}

void Gripper::setPause(boolean pause_) {
  this->pause = pause_;
}
