/*
 * Parts
 *    Servo Motor
 * Conditions & Requirements
 * 
 */

#ifndef Gripper_h
#define Gripper_h

#include "Arduino.h"
#include "config.h"
#include <Adafruit_PWMServoDriver.h>

class Gripper {
  public: 
    Adafruit_PWMServoDriver* pwmDriver;
  
    Gripper();
    Gripper(uint8_t pin_, short minAngle_, short maxAngle_, short minPulseWidth_, short maxPulseWidth_);
    void open();
    void close();
    void setAngle(short angle);
    void moveTo(short angle);
    void move();
    void setEmergency(boolean emergency_);
    void setPause(boolean pause_);
    
  private:
    short minAngle;
    short maxAngle;
    short angle;
    short pulseWidth;
    short minPulseWidth;
    short maxPulseWidth;
    uint8_t pin;
    boolean emergency = false;
    boolean pause = false;
};

#endif
