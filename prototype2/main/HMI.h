/*
 * Parts
 *   2 x LCD Display
 *   Keypad
 *   Reset button
 *   Green LED (Running Mode)
 *   Red LED   (Emergency/Error Mode)
 *   Yellow LED (Maintanence/Dry Mode)
 * Conditions & Requirements
 *   
 */

#ifndef HMI_h
#define HMI_h

#include "Arduino.h"
#include "config.h"
#include "Button.h"
#include <LiquidCrystal_I2C.h>

#define MAIN_MODE 1
#define MAINTANENCE_MODE 2
#define TEST_MODE 3

struct button {
  uint8_t pinNumber;
  String label = String(20);
};

class Item {  
  public:
    String label;
    uint8_t key;
    Item *parent;
    Item *left;
    Item *right;
    Item *first;
    
    Item() {
      label = "";
      this->parent = NULL;
      this->left = NULL;
      this->right = NULL;
      this->first = NULL;
    };

    Item(String _label) {
      this->label = _label;
      this->parent = NULL;
      this->left = NULL;
      this->right = NULL;
      this->first = NULL;
    };
};

class Menu {
  public:
    Item *root;
    Item *current;
    Item items[30];
    Item* selected;
    boolean updateScreen = true;

    Menu() {
      this->root = NULL;
      this->current = NULL;
    };

    void setFirst(Item* item, Item* rel);
    void setRight(Item* item, Item* rel);
    

    void init();
    void left();
    void right();
    void back();
    void up();
    void down();
    void set();
    void select();
    void enter();
    void yes();
    void no();
};


class HMI {
  public:
    const uint8_t ACTIVE = 1;
    const uint8_t NONACTIVE = 2;
    const uint8_t NUMBER = 3;
    const uint8_t MESSAGE = 4;
    const uint8_t HOLD = 5;
    const uint8_t LISTEN = 6;
    const uint8_t SET = 7;
    LiquidCrystal_I2C* mainDisplay;
    LiquidCrystal_I2C* statusDisplay;
    Menu* menu;
    short mode;
    HMI();

    Button* startButton;
    Button* pauseButton;
    Button* resetButton;
    Button* upButton;
    Button* downButton;
    Button* selectButton;
    Button* backButton;
    
    void init();
    void update();
    short select();
};

#endif
