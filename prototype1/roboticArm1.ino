#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// called this way, it uses the default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// you can also call it with a different address you want
//Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x41);
// you can also call it with a different address and I2C interface
//Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(&Wire, 0x40);

// Depending on your servo make, the pulse width min and max may vary, you 
// want these to be as small/large as possible without hitting the hard stop
// for max range. You'll have to tweak them as necessary to match the servos you
// have!


/*
 * Servo 1 (567 plen is mid-referred number)
 * actual minimum pulse length is 142 plen for 100Hz (225 plen is min-referred number)
 * actual maximum pulse length is 924 plen for 100Hz (910 plen is max-referred number)
 * Servo 2 (520 plen is mid-referred number)
 * actual minimum pulse length is 142 plen for 100Hz (225 plen is min-referred number)
 * actual maximum pulse length is 924 plen for 100Hz (800 plen is max-referred number)
 * Servo 3 (415 plen is mid-referred number)
 * actual minimum pulse length is 142 plen for 100Hz (145 plen is min-referred number)
 * actual maximum pulse length is 920 plen for 100Hz (750 plen is max-referred number)
 * Servo 4 (620 plen is mid-referred number)
 * actual minimum pulse length is 141 plen for 100Hz (250 plen is min-referred number)
 * actual maximum pulse length is 916 plen for 100Hz (900 plen is max-referred number)
 * Servo 5 (505 plen is mid-referred number)
 * actual minimum pulse length is 141 plen for 100Hz (155 plen is min-referred number)
 * actual maximum pulse length is 918 plen for 100Hz (860 plen is max-referred number)
 * Servo 6 (400 plen is mid-referred number)
 * actual minimum pulse length is 142 plen for 100Hz (300 plen is min-referred number)
 * actual maximum pulse length is 920 plen for 100Hz (500 plen is max-referred number)
 */

uint16_t servos[6] = {0, 1, 2, 3, 4, 5}; // using channels numbered from 0 to 5 for servos

void setup() {
  Serial.begin(9600);
  Serial.println("6 channel Servo test!");

  pwm.begin();
  
  pwm.setPWMFreq(100);  // Analog servos run at ~100 Hz updates

  delay(10);
}

String instruction = String(20);
String servoNumberString = String(20);
String pulseLengthString = String(20);
uint16_t pulseLength[6] = {567, 520, 415, 620, 505, 400}; // initial position of servos according to pulse lengths

void loop() {
  if (Serial.available() > 1) {
    /*
     * format: <servo number>,<pulse length>
     * example 1: 1,300
     * example 2: 6,450
     */
    instruction = Serial.readString(); 
    servoNumberString = instruction.substring(0, instruction.indexOf(","));
    pulseLengthString = instruction.substring(instruction.indexOf(",") + 1, instruction.length());
    pulseLength[servoNumberString.toInt() - 1] = pulseLengthString.toInt();
  }

  for (uint16_t i = 0; i < 6; i++) {
    Serial.print("--------------------- ");
    Serial.print(servos[i]);
    Serial.println(" ---------------------");
    Serial.print(pulseLength[i]);
    Serial.print(" plen");
    Serial.println();  
    pwm.setPWM(servos[i], 0, pulseLength[i]);
    delay(100);
  }
  delay(500);
}