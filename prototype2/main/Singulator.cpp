#include "Singulator.h"

Singulator::Singulator() {
  this->homePosition = 0;
  this->fullRotation = 4096;
}

Singulator::Singulator(int homePosition_, int fullRotation_) {
  this->homePosition = homePosition_;
  this->fullRotation = fullRotation_;
}

void Singulator::init() {
  this->moveToHomePosition();
  this->initialized = true;
}

void Singulator::moveToHomePosition() {  
  boolean flag = false;
  int pos = -1; 
  this->stepperMotor->setCurrentPosition(pos);
  while ( 1 ) {
      for (int i = 0; i < 3; i++) {
        if (this->infraredSensor1->isObject()) {
          flag = true;
        }
      }
      
      if (this->emergency) return;
      while (this->pause);
      this->stepperMotor->moveTo(pos--);
      this->stepperMotor->run();
      if (flag) break;
  }  
  this->stepperMotor->setCurrentPosition(this->homePosition);
}

void Singulator::moveToPosition(int position_) {
  this->stepperMotor->moveTo(position_);
  while(this->stepperMotor->distanceToGo() != 0) {
    if (this->emergency) return;
    while (this->pause);
    this->stepperMotor->run();
  }
}

void Singulator::moveFullRotation() {
  boolean flag = false;
  int pos = 1; 
  this->stepperMotor->setCurrentPosition(pos);
  while ( 1 ) {
      for (int i = 0; i < 3; i++) {
        if (this->infraredSensor1->isObject() == false) {
          flag = true;
        }
      }
      
      if (this->emergency) return;
      while (this->pause);
      this->stepperMotor->moveTo(pos++);
      this->stepperMotor->run();
      if (flag) break;
  }

  flag = false;
  delay(100);
  
  while ( 1 ) {
      for (int i = 0; i < 3; i++) {
        if (this->infraredSensor1->isObject()) {
          flag = true;
        }
      }

      if (this->emergency) return;
      while (this->pause);
      this->stepperMotor->moveTo(pos++);
      this->stepperMotor->run();
      if (flag) break;
  }
  
  this->stepperMotor->setCurrentPosition(this->homePosition);
}

void Singulator::setEmergency(boolean emergency_) {
  this->emergency = emergency_;
}

void Singulator::setPause(boolean pause_) {
  this->pause = pause_;
};
