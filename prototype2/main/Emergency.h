/*
 * Parts
 *    E-Stop  button
 * Conditions & Requirements
 *    Stop all motion
 */

#ifndef Emergency_h
#define Emergency_h

#include "Arduino.h"
#include "config.h"

void pause(int (*func)());

class EmergencyStopButton {
  public:
    const uint8_t PRESSED = 1;
    const uint8_t RELEASED = 2;
    const uint8_t CLICKED = 3;
    uint8_t state = 2;

    uint8_t pin;
    short interruptPin;

    EmergencyStopButton(uint8_t pin_, short interruptPin_);

    boolean isPressed();
    boolean isReleased();
    int read();
    
};


#endif
