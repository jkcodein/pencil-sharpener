
#ifndef LimitSwitch_h
#define LimitSwitch_h

#include "Arduino.h"
#include "config.h"

class LimitSwitch {
  public:
    uint8_t pin;
    
    LimitSwitch();
    LimitSwitch(uint8_t pin_);
    
    boolean isOpen();
    boolean isClose();
};

#endif
