#include "RobotArm.h"

RobotArm::RobotArm() {}

void RobotArm::setDefaultPulseWidth(uint16_t pulseWidth) {
  this->channel.defaultPulseWidth = pulseWidth;
  this->defaultAngle = map(
      this->channel.defaultPulseWidth, 
      this->channel.minPulseWidth, 
      this->channel.maxPulseWidth, 
      this->minAngle,
      this->maxAngle
    );
}

void RobotArm::setAngle(uint16_t degree) {
  this->angle = degree;
  this->channel.pulseWidth = map(
      this->angle, 
      this->minAngle, 
      this->maxAngle, 
      this->channel.minPulseWidth, 
      this->channel.maxPulseWidth
    );
}

void RobotArm::moveToAngle(uint16_t degree) {
  if (degree >= 0) this->setAngle(degree);
  this->sendPulseWidth(this->channel.pulseWidth);
};

void RobotArm::setEmergency(boolean emergency_) {
  this->emergency = emergency_;
};

void RobotArm::setPause(boolean pause_) {
  this->pause = pause_;
};

void RobotArm::sendPulseWidth(uint16_t pulseWidth) {
  if (this->emergency) return;
  while (this->pause);
  pwmDriver->setPWM(this->channel.number, 0, this->channel.pulseWidth);
}; 

void RobotArm::stepToDefaultAngle() {
  this->stepToAngle(this->defaultAngle);
}

void RobotArm::moveToDefaultAngle() {
  this->moveToAngle(this->defaultAngle);
}

void RobotArm::stepToAngle(uint16_t degree) {
  if (this->minAngle < degree && this->angle > degree) {
    while (this->angle > degree) {
      this->moveToAngle(this->angle - 1);
      delay(this->stepDelay);
    }
  } else if (this->maxAngle > degree && this->angle < degree) {
     while (this->angle < degree) {
      this->moveToAngle(this->angle + 1);
      delay(this->stepDelay);
    }
  }
}

Base::Base() {
  this->name = "Base";  
}

Shoulder::Shoulder() {
  this->name = "Shoulder";  
}

UpperArm::UpperArm() {
  this->name = "UpperArm";  
}

LowerArm::LowerArm() {
  this->name = "LowerArm";
}

Wrist::Wrist() {
  this->name = "Wrist";  
}    

ArmGripper::ArmGripper() {
  this->name = "Gripper";  
}

void ArmGripper::open() { // set an angle to get opened a gripper 
  this->moveToAngle(this->maxAngle);
}

void ArmGripper::close() {
  this->moveToAngle(this->minAngle);
}

RobotArmControl::RobotArmControl(){
  
  for (short i = sizeof(this->order)/sizeof(this->order[0]) - 1; i >= 0; i--) {
    this->order[i] = 0;
  }  
}

void RobotArmControl::init() {
  this->armGripper.moveToDefaultAngle();
  delay(1000);
  this->wrist.moveToDefaultAngle();
  delay(1000);
  this->lowerArm.moveToDefaultAngle();
  delay(1000);
  this->upperArm.moveToDefaultAngle();
  delay(1000);
  this->shoulder.moveToDefaultAngle();
  delay(1000);
  this->base.moveToDefaultAngle();
  delay(1000);
  this->initialized = true;
}

void RobotArmControl::moveToPositionA() {
  // position 0
  this->armGripper.open();
  this->wrist.stepToAngle(90);
  this->lowerArm.stepToAngle(102);
  this->upperArm.stepToAngle(83);
  this->shoulder.stepToAngle(86);
  this->base.stepToAngle(90);
  delay(1000);

  // position 1
  this->base.stepToAngle(9);
  this->wrist.stepToAngle(97);
  this->shoulder.stepToAngle(87);
  this->upperArm.stepToAngle(130);
  this->lowerArm.stepToAngle(125);
  delay(1000);

  this->shoulder.stepToAngle(87);
  this->upperArm.stepToAngle(161);
  this->lowerArm.stepToAngle(140);
  this->wrist.stepToAngle(90);
  delay(1000);
}

void RobotArmControl::moveToPositionI1() {
  // position 2.0
  this->shoulder.stepToAngle(85);
  this->upperArm.stepToAngle(83);
  this->lowerArm.stepToAngle(40);
  this->base.stepToAngle(82);
  this->wrist.stepToAngle(95);
  delay(1000);
}

void RobotArmControl::moveToPositionB() {
  // position 2.1
  this->lowerArm.stepToAngle(60);
  delay(1000);

  // position 2.2
  this->shoulder.stepToAngle(100);
  delay(1000);

  // position 2.3
  short degree1 = this->shoulder.angle;
  short degree2 = this->upperArm.angle;
  short degree3 = this->lowerArm.angle;
  while (1) {

    if (degree1 > 70) {
      this->shoulder.moveToAngle(degree1);
      degree1 -= 1;
      delay(15);
      if (this->shoulder.stepDelay > 70) delay(this->shoulder.stepDelay);
    }

    if (degree2 < 150) {
      for (int i = 0; i < 3;i++) {
        degree2 += 1;
        this->upperArm.moveToAngle(degree2);
        delay(5);
        if (this->upperArm.stepDelay > 70) delay(this->upperArm.stepDelay);
      }
    }

    if (degree3 < 78) {
      for (int i = 0; i < 2;i++) {
        degree3 += 1;
        this->lowerArm.moveToAngle(degree3);
        if (this->lowerArm.stepDelay > 70) delay(this->lowerArm.stepDelay);
        delay(5);
      }
    }

    if (degree1 <= 70 && degree2 >= 150 && degree3 >= 78) break;
  }
  delay(1000);
}

void RobotArmControl::moveToPositionI2() {
  // position 0
  this->shoulder.stepToAngle(60);
  delay(1000);
  
  this->wrist.stepToAngle(95);
  this->lowerArm.stepToAngle(70);
  this->upperArm.stepToAngle(120);
  this->shoulder.stepToAngle(86);
  this->upperArm.stepToAngle(100);
  this->lowerArm.stepToAngle(40);
  this->upperArm.stepToAngle(83); 
  this->base.stepToAngle(90);
  delay(1000);
}

void RobotArmControl::moveToPositionC() {

  // position 1
  this->base.stepToAngle(106);
  delay(1000);

  // position 2.1
  this->lowerArm.stepToAngle(40);
  delay(1000);

  // position 2.2
  this->shoulder.stepToAngle(100);
  delay(1000);

  // position 2.3
  short degree1 = this->shoulder.angle;
  short degree2 = this->upperArm.angle;
  short degree3 = this->lowerArm.angle;
  while (1) {

    if (degree1 > 65) {
      this->shoulder.moveToAngle(degree1);
      degree1 -= 1;
      delay(15);
      if (this->shoulder.stepDelay > 70) delay(this->shoulder.stepDelay);
    }

    if (degree2 < 179) {
      for (int i = 0; i < 3;i++) {
        degree2 += 1;
        this->upperArm.moveToAngle(degree2);
        delay(5);
      }
      if (this->upperArm.stepDelay > 70) delay(this->upperArm.stepDelay);
    }

    if (degree3 < 50) {
      for (int i = 0; i < 2;i++) {
        degree3 += 1;
        this->lowerArm.moveToAngle(degree3);
        delay(5);
      }
      if (this->lowerArm.stepDelay > 70) delay(this->lowerArm.stepDelay);
    }

    if (degree1 <= 65 && degree2 >= 179 && degree3 >= 50) break;
  }
  delay(1000);
}

void RobotArmControl::moveToPositionI2_1() {

  // position 2.3
  short degree1 = this->shoulder.angle;
  short degree2 = this->upperArm.angle;
  short degree3 = this->lowerArm.angle;
  while (1) {

    if (degree1 < 70) {
      this->shoulder.moveToAngle(degree1);
      degree1 += 1;
      delay(15);
      if (this->shoulder.stepDelay > 70) delay(this->shoulder.stepDelay);
    }

    if (degree2 > 160) {
      for (int i = 0; i < 3;i++) {
        degree2 -= 1;
        this->upperArm.moveToAngle(degree2);
        delay(5);
      }
      if (this->upperArm.stepDelay > 70) delay(this->upperArm.stepDelay);
    }

    if (degree3 > 35) {
      for (int i = 0; i < 2;i++) {
        degree3 -= 1;
        this->lowerArm.moveToAngle(degree3);
        delay(5);
      }
      if (this->lowerArm.stepDelay > 70) delay(this->lowerArm.stepDelay);
    }

    if (degree1 >= 70 && degree2 <= 160 && degree3 <= 35) break;
  }
  delay(1000);

  // position 0
  this->shoulder.stepToAngle(60);
  delay(1000);
  
  this->wrist.stepToAngle(95);
  this->lowerArm.stepToAngle(70);
  this->upperArm.stepToAngle(120);
  this->shoulder.stepToAngle(86);
  this->upperArm.stepToAngle(100);
  this->lowerArm.stepToAngle(40);
  this->upperArm.stepToAngle(83); 
  this->base.stepToAngle(90);
  delay(1000);
}

void RobotArmControl::moveToPositionD() {  

  // position 0  
  this->wrist.stepToAngle(90);
  this->lowerArm.stepToAngle(40);
  this->upperArm.stepToAngle(83);
  this->shoulder.stepToAngle(86);
  this->base.stepToAngle(90);
  delay(1000);

  // position 1
  this->base.stepToAngle(160);
  this->shoulder.stepToAngle(115);
  this->upperArm.stepToAngle(134);
  this->lowerArm.stepToAngle(40);
  delay(1000);

  this->wrist.stepToAngle(160);
  delay(1000);
}

void RobotArmControl::moveToPositionI3() {
  // position 0  
  this->upperArm.stepToAngle(83);
  this->lowerArm.stepToAngle(102);
  this->wrist.stepToAngle(90);
  this->shoulder.stepToAngle(86);
  this->base.stepToAngle(90);
  delay(1000);
}

void RobotArmControl::moveToStartSwitch() {
  this->base.stepToAngle(1);
  this->wrist.stepToAngle(90);
  this->lowerArm.stepToAngle(40);
  this->upperArm.stepToAngle(83);
  this->shoulder.stepToAngle(86);
  this->armGripper.open();

  this->wrist.stepToAngle(90);
  this->lowerArm.stepToAngle(40);
  this->upperArm.stepToAngle(158);
  this->shoulder.stepToAngle(86);
  delay(1000);

  this->lowerArm.stepToAngle(93);
  delay(1000);
  for (int i = 0; i < 10; i++) {
    if (this->startSwitch->isClose()) {
      this->start = true;
      Serial.println("SSSSSSSSTTTTTTTTTAAAAAAAAARRRRTTTTTTT");
    }
  }
  delay(5000);
  
  this->upperArm.stepToAngle(83);
  this->lowerArm.stepToAngle(40);
  this->base.stepToAngle(90);
  this->armGripper.open();
  delay(1000);
}

void RobotArmControl::setEmergency(boolean emergency_) {
  this->emergency = emergency_;
  this->base.setEmergency(emergency_);
  this->shoulder.setEmergency(emergency_);
  this->upperArm.setEmergency(emergency_);
  this->lowerArm.setEmergency(emergency_);
  this->wrist.setEmergency(emergency_);
  this->armGripper.setEmergency(emergency_);
}

void RobotArmControl::setPause(boolean pause_) {
  this->pause = pause_;
  this->base.setPause(pause_);
  this->shoulder.setPause(pause_);
  this->upperArm.setPause(pause_);
  this->lowerArm.setPause(pause_);
  this->wrist.setPause(pause_);
  this->armGripper.setPause(pause_);
}

void RobotArmControl::moveToMaintanencePosition() {
  this->armGripper.open();
  this->base.stepToAngle(160);
  this->wrist.stepToAngle(90);
  this->lowerArm.stepToAngle(120);
  this->upperArm.stepToAngle(178);
  this->shoulder.stepToAngle(93);
  delay(2000);
}
