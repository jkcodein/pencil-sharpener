/*
 * Servo 0 (567 plen is mid-referred number)
 * actual minimum pulse length is 142 plen for 100Hz (225 plen is min-referred number)
 * actual maximum pulse length is 924 plen for 100Hz (910 plen is max-referred number)
 * Servo 1 (520 plen is mid-referred number)
 * actual minimum pulse length is 142 plen for 100Hz (225 plen is min-referred number)
 * actual maximum pulse length is 924 plen for 100Hz (800 plen is max-referred point)
 * Servo 2 (415 plen is mid-referred number)
 * actual minimum pulse length is 142 plen for 100Hz (145 plen is min-referred number)
 * actual maximum pulse length is 920 plen for 100Hz (750 plen is max-referred number)
 * Servo 3 (620 plen is mid-referred number)
 * actual minimum pulse length is 141 plen for 100Hz (250 plen is min-referred number)
 * actual maximum pulse length is 916 plen for 100Hz (900 plen is max-referred number)
 * Servo 4 (505 plen is mid-referred number)
 * actual minimum pulse length is 141 plen for 100Hz (155 plen is min-referred number)
 * actual maximum pulse length is 918 plen for 100Hz (860 plen is max-referred number)
 * Servo 5 (400 plen is mid-referred number)
 * actual minimum pulse length is 142 plen for 100Hz (300 plen is min-referred number)
 * actual maximum pulse length is 920 plen for 100Hz (500 plen is max-referred number)
 */

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// called this way, it uses the default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

String instructionString = String(160);

struct channel {
  uint16_t number;
  uint16_t initialPulseLength;
  uint16_t pulseLength;
  uint16_t midPulseLength;
  uint16_t minPulseLength;
  uint16_t maxPulseLength;
} ch[6];

struct record { // data getting from joystick
  short x1;  short y1;  short x2;  short y2;
  uint16_t du;  uint16_t dd;  uint16_t dl;  uint16_t dr;
  uint16_t back;  uint16_t guide;  uint16_t start;
  uint16_t tl;  uint16_t tr;
  uint16_t a;  uint16_t b;  uint16_t x;  uint16_t y;
  uint16_t lb;  uint16_t rb;
  uint16_t lt;  uint16_t rt;
} instructionRecord, lastInstructionRecord;

struct record convertStringToRecord(const String data);
void servo0(short degree);
void servo1(short degree);
void servo2(short degree);
void servo3(short degree);
void servo4(short degree);
void servo5(short degree);

short convertDegreeToPulseLength(short degree, short slope, short intercept) {
  return slope * degree + intercept;
}

void setup() {
  Serial.begin(9600);
  Serial.println("6 channel Servo test!");

  pwm.begin();
  
  pwm.setPWMFreq(100);  // Analog servos run at ~100 Hz updates
  ch[0].number = 0;
  ch[0].initialPulseLength = 567;
  ch[0].pulseLength = 567;
  ch[0].midPulseLength = 567;
  ch[0].minPulseLength = 225;
  ch[0].maxPulseLength = 910;
  ch[1].number = 1;
  ch[1].initialPulseLength = 520;
  ch[1].pulseLength = 520;
  ch[1].midPulseLength = 520;
  ch[1].minPulseLength = 225;
  ch[1].maxPulseLength = 800;
  ch[2].number = 2;
  ch[2].initialPulseLength = 415;
  ch[2].pulseLength = 415;
  ch[2].midPulseLength = 415;
  ch[2].minPulseLength = 145;
  ch[2].maxPulseLength = 750;
  ch[3].number = 3;
  ch[3].initialPulseLength = 620;
  ch[3].pulseLength = 620;
  ch[3].midPulseLength = 620;
  ch[3].minPulseLength = 250;
  ch[3].maxPulseLength = 900;
  ch[4].number = 4;
  ch[4].initialPulseLength = 505;
  ch[4].pulseLength = 505;
  ch[4].midPulseLength = 505;
  ch[4].minPulseLength = 155;
  ch[4].maxPulseLength = 860;
  ch[5].number = 5;
  ch[5].initialPulseLength = 400;
  ch[5].pulseLength = 400;
  ch[5].midPulseLength = 400;
  ch[5].minPulseLength = 300;
  ch[5].maxPulseLength = 500;
  
  for (uint16_t i = 0; i < 6; i++) {
    pwm.setPWM(ch[i].number, 0, ch[i].initialPulseLength);
    delay(1000);
  }

  delay(10);
}

short steps[6] = {0, 0, 0, 0, 0, 0};

void loop() {
    if (Serial.available() > 0) {
      instructionString = Serial.readStringUntil('\n');
      instructionRecord = convertStringToRecord(instructionString);
    }

  if (instructionRecord.back == 1) {
    steps[0] = -1.5;
  } else if (instructionRecord.start == 1) {
    steps[0] = 1.5;
  } else {
    steps[0] = 0;
  }

  if (steps[0] < 0) {
    if (ch[0].pulseLength > ch[0].minPulseLength) {
      pwm.setPWM(ch[0].number, 0, ch[0].pulseLength);
      ch[0].pulseLength += steps[0];
      delay(10);
    }   
  } else if (steps[0] > 0) {
    if (ch[0].pulseLength < ch[0].maxPulseLength) {
      pwm.setPWM(ch[0].number, 0, ch[0].pulseLength);
      ch[0].pulseLength += steps[0];
      delay(10);
    }
  }
  
  if (instructionRecord.y == 1) {
    steps[1] = -1.5;
  } else if (instructionRecord.a == 1) {
    steps[1] = 1.5;
  } else {
    steps[1] = 0;
  }

  if (steps[1] < 0) {
    if (ch[1].pulseLength > ch[1].minPulseLength) {
      pwm.setPWM(ch[1].number, 0, ch[1].pulseLength);
      ch[1].pulseLength += steps[1];
      delay(10);
    }   
  } else if (steps[1] > 0) {
    if (ch[1].pulseLength < ch[1].maxPulseLength) {
      pwm.setPWM(ch[1].number, 0, ch[1].pulseLength);
      ch[1].pulseLength += steps[1];
      delay(10);
    }
  }

  if (instructionRecord.x == 1) {
    steps[2] = -1.5;
  } else if (instructionRecord.b == 1) {
    steps[2] = 1.5;
  } else {
    steps[2] = 0;
  }

  if (steps[2] < 0) {
    if (ch[2].pulseLength > ch[2].minPulseLength) {
      pwm.setPWM(ch[2].number, 0, ch[2].pulseLength);
      ch[2].pulseLength += steps[2];
      delay(10);
    }   
  } else if (steps[2] > 0) {
    if (ch[2].pulseLength < ch[2].maxPulseLength) {
      pwm.setPWM(ch[2].number, 0, ch[2].pulseLength);
      ch[2].pulseLength += steps[2];
      delay(10);
    }
  }

  if (instructionRecord.du == 1) {
    steps[3] = -1.5;
  } else if (instructionRecord.dd == 1) {
    steps[3] = 1.5;
  } else {
    steps[3] = 0;
  }

  if (steps[3] < 0) {
    if (ch[3].pulseLength > ch[3].minPulseLength) {
      pwm.setPWM(ch[3].number, 0, ch[3].pulseLength);
      ch[3].pulseLength += steps[3];
    }   
  } else if (steps[3] > 0) {
    if (ch[3].pulseLength < ch[3].maxPulseLength) {
      pwm.setPWM(ch[3].number, 0, ch[3].pulseLength);
      ch[3].pulseLength += steps[3];
    }
  }
  
  if (instructionRecord.dl == 1) {
    steps[4] = -1.5;
  } else if (instructionRecord.dr == 1) {
    steps[4] = 1.5;
  } else {
    steps[4] = 0;
  }

  if (steps[4] < 0) {
    if (ch[4].pulseLength > ch[4].minPulseLength) {
      pwm.setPWM(ch[4].number, 0, ch[4].pulseLength);
      ch[4].pulseLength += steps[4];
    }   
  } else if (steps[4] > 0) {
    if (ch[4].pulseLength < ch[4].maxPulseLength) {
      pwm.setPWM(ch[4].number, 0, ch[4].pulseLength);
      ch[4].pulseLength += steps[4];
    }
  }  

  if (instructionRecord.lb == 1) {
    steps[5] = -1.5;
  } else if (instructionRecord.rb == 1) {
    steps[5] = 1.5;
  } else {
    steps[5] = 0;
  }

  if (steps[5] < 0) {
    if (ch[5].pulseLength > ch[5].minPulseLength) {
      pwm.setPWM(ch[5].number, 0, ch[5].pulseLength);
      ch[5].pulseLength += steps[5];
      delay(10);
    }   
  } else if (steps[5] > 0) {
    if (ch[5].pulseLength < ch[5].maxPulseLength) {
      pwm.setPWM(ch[5].number, 0, ch[5].pulseLength);
      ch[5].pulseLength += steps[5];
      delay(10);
    }
  }

  delay(3);
}


/*
  X1:  1561 
  Y1:   161  
  X2:    56 
  Y2:  -861  
  du:0 
  dd:0 
  dl:0 
  dr:0  
  back:0 
  guide:0 
  start:0  
  TL:0 
  TR:0  
  A:0 
  B:0 
  X:0 
  Y:0  
  LB:0 
  RB:0  
  LT:  0 
  RT:  0
*/
struct record convertStringToRecord(const String data) {
  String tmp = data;
  short index[21];
  
  index[0] = tmp.indexOf("X1:", 0);
  index[1] = tmp.indexOf("Y1:", index[0] + 1);
  index[2] = tmp.indexOf("X2:", index[1] + 1);
  index[3] = tmp.indexOf("Y2:", index[2] + 1);
  index[4] = tmp.indexOf("du:", index[3] + 1);
  index[5] = tmp.indexOf("dd:", index[4] + 1);
  index[6] = tmp.indexOf("dl:", index[5] + 1);
  index[7] = tmp.indexOf("dr:", index[6] + 1);
  index[8] = tmp.indexOf("back:", index[7] + 1);
  index[9] = tmp.indexOf("guide:", index[8] + 1);
  index[10] = tmp.indexOf("start:", index[9] + 1);
  index[11] = tmp.indexOf("TL:", index[10] + 1);
  index[12] = tmp.indexOf("TR:", index[11] + 1);
  index[13] = tmp.indexOf("A:", index[12] + 1);
  index[14] = tmp.indexOf("B:", index[13] + 1);
  index[15] = tmp.indexOf("X:", index[14] + 1);
  index[16] = tmp.indexOf("Y:", index[15] + 1);
  index[17] = tmp.indexOf("LB:", index[16] + 1);
  index[18] = tmp.indexOf("RB:", index[17] + 1);
  index[19] = tmp.indexOf("LT:", index[18] + 1);
  index[20] = tmp.indexOf("RT:", index[19] + 1);

  record res;
  res.x1 = tmp.substring(index[0] + 3, index[1]).toInt();
  res.y1 = tmp.substring(index[1] + 3, index[2]).toInt();
  res.x2 = tmp.substring(index[2] + 3, index[3]).toInt();
  res.y2 = tmp.substring(index[3] + 3, index[4]).toInt();
  res.du = tmp.substring(index[4] + 3, index[5]).toInt();
  res.dd = tmp.substring(index[5] + 3, index[6]).toInt();
  res.dl = tmp.substring(index[6] + 3, index[7]).toInt();
  res.dr = tmp.substring(index[7] + 3, index[8]).toInt();
  res.back = tmp.substring(index[8] + 5, index[9]).toInt();
  res.guide = tmp.substring(index[9] + 6, index[10]).toInt();
  res.start = tmp.substring(index[10] + 6, index[11]).toInt();
  res.tl = tmp.substring(index[11] + 3, index[12]).toInt();
  res.tr = tmp.substring(index[12] + 3, index[13]).toInt();
  res.a = tmp.substring(index[13] + 2, index[14]).toInt();
  res.b = tmp.substring(index[14] + 2, index[15]).toInt();
  res.x = tmp.substring(index[15] + 2, index[16]).toInt();
  res.y = tmp.substring(index[16] + 2, index[17]).toInt();
  res.lb = tmp.substring(index[17] + 3, index[18]).toInt();
  res.rb = tmp.substring(index[18] + 3, index[19]).toInt();
  res.lt = tmp.substring(index[19] + 3, index[20]).toInt();
  res.rt = tmp.substring(index[20] + 3).toInt();

  return res;
}