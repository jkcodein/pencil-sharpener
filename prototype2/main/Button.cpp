#include "Button.h"

Button::Button(uint8_t pin_) {
  this->pin = pin_;
  this->mode = INPUT_PULLUP;
  pinMode(this->pin, INPUT_PULLUP);
}

Button::Button(uint8_t pin_, uint8_t mode_) {
  this->pin = pin_;
  this->mode = mode_;
  pinMode(this->pin, this->mode);
}

boolean Button::read() {
  return digitalRead(this->pin);
}

boolean Button::isPressed() {
  boolean val = digitalRead(this->pin);
  if (val == LOW) {
    this->state = this->PRESSED;
    return true;  
  }
  
  return false;
}

boolean Button::isReleased() {
  boolean val = digitalRead(this->pin);
  if (val == HIGH && this->state == this->PRESSED) {
    this->state = this->RELEASED;
    return true;
  }
  
  return false;
}

boolean Button::isClicked() {
  short val = -1;
  boolean state = 0;
  while (1) {
    val = digitalRead(this->pin);
    if (val == LOW) {
      state = this->PRESSED;
    }
    
    if (val == HIGH && state == this->PRESSED) {
      delay(100);
      return true;
    } 

    if (val == HIGH && state == 0) {
      break;
    }
  }
  return false;
}
