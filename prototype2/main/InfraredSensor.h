#ifndef InfraredSensor_h
#define InfraredSensor_h

#include "Arduino.h"
#include "config.h"

class InfraredSensor {
  public:
    uint8_t pin;
    uint8_t analogPin;
    
    InfraredSensor();
    InfraredSensor(uint8_t pin_);
    InfraredSensor(uint8_t digitalPin_, uint8_t analogPin_);
    
    boolean isObject();
    boolean isFilled();
};

#endif
