#ifndef Logger_h
#define Logger_h

#include "Arduino.h"
#include "config.h"

void println(String message) {
  if (DEBUG) {
    Serial.println(message);
    delay(100);
  }
}


#endif
