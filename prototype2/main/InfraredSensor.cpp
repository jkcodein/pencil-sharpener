#include "InfraredSensor.h"

InfraredSensor::InfraredSensor() {

}

InfraredSensor::InfraredSensor(uint8_t pin_) {
  this->pin = pin_;
  pinMode(this->pin, INPUT_PULLUP);
}

InfraredSensor::InfraredSensor(uint8_t digitalPin_, uint8_t analogPin_) {
  this->pin = digitalPin_;
  this->analogPin = analogPin_;
  pinMode(this->pin, INPUT_PULLUP);
  pinMode(this->analogPin, INPUT);
}

boolean InfraredSensor::isObject() {
  if (this->pin > 0 && this->analogPin > 0)  
    return digitalRead(this->pin) == LOW || analogRead(this->analogPin) < 800;
  else
    return digitalRead(this->pin) == LOW;
}

boolean InfraredSensor::isFilled() {
  return digitalRead(this->pin) == LOW && analogRead(this->analogPin) < 800;
}
