/*
  Robotic Arm consists of six servos used to drive different parts
  such as rotary base, shoulder (column), upper arm, lower arm, wrist, and gripper.
  All parts have own limitation in servo's angle of rotation. 

  A RobotArm class is a parent class for other classes 
  such as Base, Shoulder, UpperArm, LowerArm, Wrist, and Gripper.
  All child classes inherit properties (variable within a class) and methods (function within a class) of parent class.
  A child class can have own methods and override methods of parent class.

  The struct data type PCABoardChannel is combination of variables 
  for a channel of the board "PCA9685 16-channel". 
  It is used to store the pulse widths that are set on servos.

  A generated pulse width is used to control servos or rotate to certain angle.
*/

/*
  Measurements for servos depending on pulse width:
    Servo 0 (567 pwidth is default number)
    actual minimum pulse length is 142 pwidth for 100Hz (225 pwidth is min-referred number)
    actual maximum pulse length is 924 pwidth for 100Hz (910 pwidth is max-referred number)
    Servo 1 (520 pwidth is default number)
    actual minimum pulse length is 142 pwidth for 100Hz (225 pwidth is min-referred number)
    actual maximum pulse length is 924 pwidth for 100Hz (800 pwidth is max-referred point)
    Servo 2 (415 pwidth is default number)
    actual minimum pulse length is 142 pwidth for 100Hz (145 pwidth is min-referred number)
    actual maximum pulse length is 920 pwidth for 100Hz (750 pwidth is max-referred number)
    Servo 3 (620 pwidth is default number)
    actual minimum pulse length is 141 pwidth for 100Hz (250 pwidth is min-referred number)
    actual maximum pulse length is 916 pwidth for 100Hz (900 pwidth is max-referred number)
    Servo 4 (505 pwidth is default number)
    actual minimum pulse length is 141 pwidth for 100Hz (155 pwidth is min-referred number)
    actual maximum pulse length is 918 pwidth for 100Hz (860 pwidth is max-referred number)
    Servo 5 (400 pwidth is default number)
    actual minimum pulse length is 142 pwidth for 100Hz (300 pwidth is min-referred number)
    actual maximum pulse length is 920 pwidth for 100Hz (500 pwidth is max-referred number)
*/

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// called this way, it uses the default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

struct PCABoardChannel {
  uint8_t number;
  uint16_t defaultPulseWidth;  
  uint16_t pulseWidth;
  uint16_t minPulseWidth;
  uint16_t maxPulseWidth;
};

class RobotArm {
  private:
    uint16_t defaultAngle;
  public:
    Adafruit_PWMServoDriver pwmDriver;
    PCABoardChannel channel;
    uint16_t minAngle;
    uint16_t maxAngle;
    uint16_t angle;
    uint16_t stepDelay;
  
    RobotArm() {};

    void setDefaultAngle(uint16_t degree) {
      this->defaultAngle = degree;
      this->setAngle(this->defaultAngle);
    }

    void setAngle(uint16_t degree) {
      this->angle = degree;
      this->channel.pulseWidth = map(degree, this->minAngle, this->maxAngle, this->channel.minPulseWidth, this->channel.maxPulseWidth);
      this->setPulseWidth(this->channel.pulseWidth);
    };

    void setPulseWidth(uint16_t pulseWidth) {
      this->channel.pulseWidth = pulseWidth;
      pwm.setPWM(this->channel.number, 0, this->channel.pulseWidth);
      Serial.println(pulseWidth);
    };  

    void stepToAngle(uint16_t degree) {
      if (this->minAngle < degree && this->angle > degree) {
        while (this->angle > degree) {
          this->setAngle(this->angle - 1);
          delay(this->stepDelay);
        }
      } else if (this->maxAngle > degree && this->angle < degree) {
         while (this->angle < degree) {
          this->setAngle(this->angle + 1);
          delay(this->stepDelay);
        }
      }
    }  
};

class Base : public RobotArm { // inherit properties and methods of the RobotArm class
  public: 
    Base() {};
};

class Shoulder : public RobotArm {
  public:
    Shoulder() {};
};

class UpperArm : public RobotArm {
  public:
    UpperArm() {};
};

class LowerArm : public RobotArm {
  public:
    LowerArm() {};
};

class Wrist : public RobotArm {
  public:
    Wrist() {};    
};

class Gripper : public RobotArm {
  public:
    Gripper() {};

    void open() { // set an angle to get opened a gripper 
      this->setAngle(this->minAngle);
    }

    void close() {
      this->setAngle(this->maxAngle);
    }
};

Base base;         // declare object base as instance of the Base class
Shoulder shoulder; // declare object shoulder as instance of the Shoulder class
UpperArm upperArm;
LowerArm lowerArm;
Wrist wrist;
Gripper gripper;

void setup() {
  Serial.begin(9600);
  Serial.println("6 channel Servo test!");

  pwm.begin();

  pwm.setPWMFreq(100);  // Analog servos run at ~100 Hz updates

  base.pwmDriver = pwm;
  base.minAngle = 0;
  base.maxAngle = 180;
  base.channel.number = 0;
  base.channel.defaultPulseWidth = 567;
  base.channel.minPulseWidth = 225;
  base.channel.maxPulseWidth = 910;
  base.stepDelay = 15; // milliseconds

  shoulder.pwmDriver = pwm;
  shoulder.minAngle = 30;
  shoulder.maxAngle = 150;
  shoulder.channel.number = 1;
  shoulder.channel.defaultPulseWidth = 520;
  shoulder.channel.minPulseWidth = 250;
  shoulder.channel.maxPulseWidth = 800;
  shoulder.stepDelay = 50; // milliseconds

  upperArm.pwmDriver = pwm;
  upperArm.minAngle = 0;
  upperArm.maxAngle = 180;
  upperArm.channel.number = 2;
  upperArm.channel.defaultPulseWidth = 415;
  upperArm.channel.minPulseWidth = 145;
  upperArm.channel.maxPulseWidth = 750;
  upperArm.stepDelay = 15; // milliseconds

  lowerArm.pwmDriver = pwm;
  lowerArm.minAngle = 0;
  lowerArm.maxAngle = 180;
  lowerArm.channel.number = 3;
  lowerArm.channel.defaultPulseWidth = 620;
  lowerArm.channel.minPulseWidth = 250;
  lowerArm.channel.maxPulseWidth = 900;
  lowerArm.stepDelay = 15; // milliseconds

  wrist.pwmDriver = pwm;
  wrist.minAngle = 0;
  wrist.maxAngle = 180;
  wrist.channel.number = 4;
  wrist.channel.defaultPulseWidth = 505;
  wrist.channel.minPulseWidth = 155;
  wrist.channel.maxPulseWidth = 860;
  wrist.stepDelay = 15; // milliseconds
  
  gripper.pwmDriver = pwm;
  gripper.minAngle = 10;
  gripper.maxAngle = 60;
  gripper.channel.number = 5;
  gripper.channel.defaultPulseWidth = 400;
  gripper.channel.minPulseWidth = 300;
  gripper.channel.maxPulseWidth = 500;
  gripper.stepDelay = 15; // milliseconds

  base.setDefaultAngle(90);
  delay(1000);
  shoulder.setDefaultAngle(90);
  delay(1000);
  upperArm.setDefaultAngle(84);
  delay(1000);
  lowerArm.setDefaultAngle(102);
  delay(1000);
  wrist.setDefaultAngle(90);
  delay(1000);
  gripper.setDefaultAngle(35);
  delay(1000);
  
  delay(10);
}


void loop() {
  
  if (Serial.available() > 1) {
    int key = Serial.parseInt();
    switch (key) {
      case 1: 
        base.stepToAngle(90);
        shoulder.stepToAngle(70);
        upperArm.stepToAngle(30);
        lowerArm.stepToAngle(10);
        wrist.stepToAngle(0);
        gripper.close();
        break;
      case 2:     
        gripper.open();
        wrist.stepToAngle(90);
        lowerArm.stepToAngle(102);
        upperArm.stepToAngle(84);
        shoulder.stepToAngle(90);
        base.stepToAngle(90);
        break;
      case 3:     
        gripper.close();
        break;
      case 4:     
        gripper.open();
        break;
    }
  }
  
  delay(3);
}