#ifndef Button_h
#define Button_h

#include "Arduino.h"

class Button {
  public:
    const uint8_t PRESSED = 1;
    const uint8_t RELEASED = 2;
    const uint8_t CLICKED = 3;
    uint8_t state = 2;
    
    uint8_t pin;
    uint8_t mode;
    
    Button(uint8_t pin_);
    Button(uint8_t pin_, uint8_t mode_);

    boolean read();
    boolean isPressed();
    boolean isReleased();
    boolean isClicked();
};

#endif
