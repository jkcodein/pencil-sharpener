#include "Actuator.h"
#include "Emergency.h"

Actuator::Actuator() {
  this->homePosition = 0;
}

Actuator::Actuator(int homePosition_, int maxPosition_) {
  this->homePosition = homePosition_;
  this->maxPosition = maxPosition_;
}

void Actuator::init() {
  this->gripper->open();
  this->moveToHomePosition();
  this->initialized = true;
}

void Actuator::moveToHomePosition() {
  long pos = -1;
  while (this->homeLimitSwitch->isOpen()) {
    if (this->emergency) return;
    while (this->pause);
    this->stepperMotor->moveTo(pos--);
    this->stepperMotor->run();
  }
  this->stepperMotor->setCurrentPosition(this->homePosition);
}

void Actuator::moveToMaxPosition() {
  this->stepperMotor->moveTo(this->maxPosition);
  while(this->stepperMotor->distanceToGo() != 0) {
    if (this->emergency) return;
    while (this->pause);
    this->stepperMotor->run();
  }
}

void Actuator::moveToPosition(int position_) {
  this->stepperMotor->moveTo(position_);
  while(this->stepperMotor->distanceToGo() != 0) {
    if (this->emergency) return;
    while (this->pause);
    this->stepperMotor->run();
  }
}

void Actuator::setEmergency(boolean emergency_) {
  this->emergency = emergency_;
  this->gripper->setEmergency(emergency_);
}

void Actuator::setPause(boolean pause_) {
  this->pause = pause_;
  this->gripper->setPause(pause_);
}
